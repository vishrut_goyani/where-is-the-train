package com.demo.whereismytrain.interfaces;

public interface OnItemClicked {
    void onItemClick(int position);
}
