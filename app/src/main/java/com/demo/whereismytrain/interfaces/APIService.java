package com.demo.whereismytrain.interfaces;

import com.demo.whereismytrain.CustomClass;
import com.demo.whereismytrain.model.OfficeModel;
import com.demo.whereismytrain.model.traindao.AllTrainModel;
import com.demo.whereismytrain.model.traindao.CancelledModel;
import com.demo.whereismytrain.model.traindao.CoachModel;
import com.demo.whereismytrain.model.traindao.AllStationModel;
import com.demo.whereismytrain.model.traindao.DivertedModel;
import com.demo.whereismytrain.model.traindao.PartiallyCancelledModel;
import com.demo.whereismytrain.model.traindao.RescheduledModel;
import com.demo.whereismytrain.model.traindao.SearchByNoModel;
import com.demo.whereismytrain.model.traindao.StationTrainsModel;
import com.demo.whereismytrain.model.traindao.TrainNoModel;
import com.demo.whereismytrain.utils.TrainUtils;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;

public interface APIService {

    @GET(TrainUtils.ALL_TRAIN)
    Call<AllTrainModel> getAllTrains();

    @GET(TrainUtils.ALL_STATION)
    Call<AllStationModel> getAllStations();

    @GET("{slug}")
    Call<CancelledModel> getCancelledTrains(
            @Path(value = "slug", encoded = true)
                    String date);

    @GET("{slug}")
    Call<PartiallyCancelledModel> getPartiallyCancelledCustomTrains(
            @Path(value = "slug", encoded = true)
                    String date);

    @GET("{slug}")
    Call<DivertedModel> getDivertedTrains(
            @Path(value = "slug", encoded = true)
                    String date);

    @GET("{slug}")
    Call<RescheduledModel> getRescheduledTrains(
            @Path(value = "slug", encoded = true)
                    String date);

    @GET("TrainInformation/apikey/" + TrainUtils.API_KEY + "/TrainNumber/" + "{slug}")
    Call<TrainNoModel> getTrainInfoByNo(
            @Path(value = "slug", encoded = true)
                    String trainno);

    @FormUrlEncoded
    @POST("train-list")
    Call<SearchByNoModel> getTrainInfoAutoComplete(
            @Field("train_no") String trainno);

    @FormUrlEncoded
    @POST("station-list")
    Call<AllStationModel> getStationAutoComplete(
            @Field("station") String stationcode);

    @FormUrlEncoded
    @POST("station-from-code")
    Call<AllStationModel> getTrainStationInfo(
            @Field("station") String trainno);

    @GET("StationCode/{slug}")
    Call<StationTrainsModel> getStationTrainsList(
            @Path(value = "slug", encoded = true) String stationCode);

    @GET("TrainNumber/{slug}")
    Call<CoachModel> getCoachInfo(
            @Path(value = "slug", encoded = true) String trainno);

    @Multipart
    @POST("convert-batch-win.php")
    Call<OfficeModel> getConvertedPdf(
            @Part("file") RequestBody requestBody
//            @Part("filelocation") RequestBody location
    );

}
