package com.demo.whereismytrain.adapter;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.demo.whereismytrain.R;
import com.demo.whereismytrain.activity.TrainsByNoActivity;
import com.demo.whereismytrain.model.traindao.CancelledModel;
import com.demo.whereismytrain.widgets.BaseModulesAdapter;

import java.util.ArrayList;

public class CancelledTrainsAdapter extends BaseModulesAdapter<SearchTrainsAdapter.ItemHolder> {

    Context context;
    ArrayList<CancelledModel.Trains_Details> cancelledList;
    private static final int TYPE_HEADER = 0;
    private static final int TYPE_ITEM = 1;
    public static final String TRAIN_CODE = "train_code";
    public static final String TRAIN_NAME = "train_name";
    public static final String TRAIN_SOURCE = "train_source";
    public static final String TIME_DURATION = "train_source_time";
    public static final String TRAIN_DESTINATION = "train_destination";
    public static final String TRAIN_DESTINATION_TIME = "train_destination_time";

    public CancelledTrainsAdapter(Context context, ArrayList<CancelledModel.Trains_Details> searchList) {
        this.context = context;
        this.cancelledList = searchList;
    }

    @Override
    public SearchTrainsAdapter.ItemHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.item_search_by_no, parent, false);
        return new SearchTrainsAdapter.ItemHolder(v);
    }

    @Override
    public void onBindViewHolder(SearchTrainsAdapter.ItemHolder holder, int position) {

        try {
            CancelledModel.Trains_Details searchByNoModel = cancelledList.get(position);
            holder.num_train.setText(searchByNoModel.getTrainNumber());
            holder.name_train.setText(searchByNoModel.getTrainName());

            holder.stationsFrom.setText(searchByNoModel.getSource());
            holder.stationsTo.setText(searchByNoModel.getDestination());
            Log.e("TAG", "onBind: " + searchByNoModel.getTrainName());

//            getStationName(searchByNoModel.getTrain_source(), holder.stationsFrom);
//            getStationName(searchByNoModel.getDestination_source(), holder.stationsTo);

            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Log.e("TAG", "onClick: " + "Clicked" + position);

                    Intent intent = new Intent(context, TrainsByNoActivity.class)
                            .putExtra(TRAIN_CODE, searchByNoModel.getTrainNumber())
                            .putExtra(TRAIN_NAME, searchByNoModel.getTrainName())
                            .putExtra(TRAIN_SOURCE, searchByNoModel.getSource())
                            .putExtra(TRAIN_DESTINATION, searchByNoModel.getDestination())
                            .putExtra(TIME_DURATION, searchByNoModel.getSource());
                    context.startActivity(intent);
                }
            });
        } catch (Exception e) {
            Log.e("TAG", "onBindViewHolder: " + e.getMessage());
        }

    }

    public static class ItemHolder extends RecyclerView.ViewHolder {

        View header_view;
        TextView num_train, name_train, stationsFrom, stationsTo;

        public ItemHolder(@NonNull View itemView) {
            super(itemView);

            header_view = itemView.findViewById(R.id.header_view);
            num_train = itemView.findViewById(R.id.num_train);
            name_train = itemView.findViewById(R.id.name_train);
            stationsFrom = itemView.findViewById(R.id.stationsFrom);
            stationsTo = itemView.findViewById(R.id.stationsTo);

        }

    }

    public void updateSearchResults(ArrayList<CancelledModel.Trains_Details> searchResults) {
        this.cancelledList = searchResults;
    }

    @Override
    public int getItemCount() {
        return cancelledList.size();
    }

}
