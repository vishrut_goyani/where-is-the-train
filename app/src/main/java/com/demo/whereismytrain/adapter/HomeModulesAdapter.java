package com.demo.whereismytrain.adapter;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityOptionsCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.demo.whereismytrain.R;
import com.demo.whereismytrain.activity.HomeMainActivity;
import com.demo.whereismytrain.activity.OtherTrainsActivity;
import com.demo.whereismytrain.activity.SearchStationsActivity;
import com.demo.whereismytrain.activity.SearchTrainsActivity;
import com.demo.whereismytrain.model.HomeScreenModules;
import com.demo.whereismytrain.widgets.BaseModulesAdapter;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.datepicker.CalendarConstraints;
import com.google.android.material.datepicker.CompositeDateValidator;
import com.google.android.material.datepicker.DateValidatorPointBackward;
import com.google.android.material.datepicker.DateValidatorPointForward;
import com.google.android.material.datepicker.MaterialDatePicker;
import com.google.android.material.datepicker.MaterialPickerOnPositiveButtonClickListener;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

public class HomeModulesAdapter extends BaseModulesAdapter<HomeModulesAdapter.ItemHolder> {

    Context context;
    ArrayList<HomeScreenModules> arrayList;
    public static final String MODULE_TYPE = "module_type";
    public static final String CHOSEN_DATE = "chosen_date";
    public static final String TRAIN_STATUS_TYPE = "train_status_type";

    public HomeModulesAdapter(Context context, ArrayList<HomeScreenModules> arrayList) {
        this.context = context;
        this.arrayList = arrayList;
    }


    @Override
    public ItemHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.item_home_modules_new, parent, false);
        return new ItemHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ItemHolder holder, int position) {
        HomeScreenModules homeScreenModules = arrayList.get(position);
//        holder.item_image.setImageResource(homeScreenModules.getImage());
//        holder.item_text.setText(homeScreenModules.getNameText());
//
//        holder.item_card.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//
//            }
//        });

        holder.item_button.setText(homeScreenModules.getNameText());
        holder.item_button
                .setCompoundDrawablesWithIntrinsicBounds(0, homeScreenModules.getImage(), 0, 0);
    }

    public static class ItemHolder extends RecyclerView.ViewHolder {

        //        androidx.appcompat.widget.AppCompatImageView item_image;
//        TextView item_text;
//        MaterialCardView item_card;
        MaterialButton item_button;

        public ItemHolder(@NonNull final View itemView) {
            super(itemView);

//            item_text = itemView.findViewById(R.id.item_text);
//            item_image = itemView.findViewById(R.id.item_image);
//            item_card = itemView.findViewById(R.id.item_card);
            item_button = itemView.findViewById(R.id.item_button);
            item_button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    switch (getAdapterPosition()) {
                        case 0:
                            ActivityOptionsCompat options = ActivityOptionsCompat.
                                    makeSceneTransitionAnimation(
                                            (HomeMainActivity) itemView.getContext(),
                                            item_button
                                            , "searchTrain");
                            itemView.getContext().startActivity(
                                    new Intent(itemView.getContext(), SearchTrainsActivity.class)
                                            .putExtra(MODULE_TYPE, "TrainsByNoActivity"));
                            break;
                        case 1:
                            itemView.getContext().startActivity(
                                    new Intent(itemView.getContext(), SearchStationsActivity.class)
                                            .putExtra(MODULE_TYPE, "StationStatusActivity"));
                            break;
                        case 3:
                            itemView.getContext().startActivity(
                                    new Intent(itemView.getContext(), SearchTrainsActivity.class)
                                            .putExtra(MODULE_TYPE, "CoachActivity"));
                            break;
                        case 5:
                            showDatePicker("cancelled");
                            break;
                        case 6:
                            showDatePicker("partially_cancelled");
                            break;
                        case 7:
                            showDatePicker("diverted");
                            break;
                        case 8:
                            showDatePicker("rescheduled");
                            break;
                    }
                }
            });
        }

        private void showDatePicker(String type) {
            MaterialDatePicker.Builder builder = MaterialDatePicker.Builder.datePicker();
            builder.setTitleText("Choose a Date");
            long today = MaterialDatePicker.todayInUtcMilliseconds();
            Calendar calendar = Calendar.getInstance();
            calendar.setTimeInMillis(today);

            Calendar cal = Calendar.getInstance();
            long start = cal.getTimeInMillis();
            calendar.set(Calendar.MONTH, calendar.get(Calendar.MONTH) + 2);
            long end = calendar.getTimeInMillis();

            builder.setSelection(today);
            CalendarConstraints.DateValidator dateValidatorMin = DateValidatorPointForward.from(today);
            CalendarConstraints.DateValidator dateValidatorMax = DateValidatorPointBackward.before(end);

            ArrayList<CalendarConstraints.DateValidator> listValidators =
                    new ArrayList<CalendarConstraints.DateValidator>();
            listValidators.add(dateValidatorMin);
            listValidators.add(dateValidatorMax);

            CalendarConstraints.Builder constraintsBuilderRange = new CalendarConstraints.Builder();
            CalendarConstraints.DateValidator validators = CompositeDateValidator.allOf(listValidators);
            constraintsBuilderRange.setValidator(validators);
            constraintsBuilderRange.setStart(start);
            constraintsBuilderRange.setEnd(end);

            builder.setCalendarConstraints(constraintsBuilderRange.build());

            MaterialDatePicker materialDatePicker = builder.build();
            materialDatePicker.show(((HomeMainActivity) itemView.getContext()).getSupportFragmentManager(), "DATE_CHOOSER");

            materialDatePicker.addOnPositiveButtonClickListener(new MaterialPickerOnPositiveButtonClickListener() {
                @Override
                public void onPositiveButtonClick(Object selection) {
                    switch (type) {
                        case "cancelled":
                            itemView.getContext().startActivity(
                                    new Intent(itemView.getContext(), OtherTrainsActivity.class)
                                            .putExtra(MODULE_TYPE, "OtherTrainsActivity")
                                            .putExtra(TRAIN_STATUS_TYPE, "cancelled")
                                            .putExtra(CHOSEN_DATE, getDate(getMillisOfDate(materialDatePicker.getHeaderText()), "yyyyMMdd")));
                            break;
                        case "partially_cancelled":
                            itemView.getContext().startActivity(
                                    new Intent(itemView.getContext(), OtherTrainsActivity.class)
                                            .putExtra(MODULE_TYPE, "OtherTrainsActivity")
                                            .putExtra(TRAIN_STATUS_TYPE, "partially_cancelled")
                                            .putExtra(CHOSEN_DATE, getDate(getMillisOfDate(materialDatePicker.getHeaderText()), "yyyyMMdd")));
                            break;
                        case "diverted":
                            itemView.getContext().startActivity(
                                    new Intent(itemView.getContext(), OtherTrainsActivity.class)
                                            .putExtra(MODULE_TYPE, "OtherTrainsActivity")
                                            .putExtra(TRAIN_STATUS_TYPE, "diverted")
                                            .putExtra(CHOSEN_DATE, getDate(getMillisOfDate(materialDatePicker.getHeaderText()), "yyyyMMdd")));
                            break;
                        case "rescheduled":
                            itemView.getContext().startActivity(
                                    new Intent(itemView.getContext(), OtherTrainsActivity.class)
                                            .putExtra(MODULE_TYPE, "OtherTrainsActivity")
                                            .putExtra(TRAIN_STATUS_TYPE, "rescheduled")
                                            .putExtra(CHOSEN_DATE, getDate(getMillisOfDate(materialDatePicker.getHeaderText()), "yyyyMMdd")));
                            break;
                    }
                }
            });
        }

        public static String getDate(long milliSeconds, String dateFormat) {
            // Create a DateFormatter object for displaying date in specified format.
            SimpleDateFormat formatter = new SimpleDateFormat(dateFormat);

            // Create a calendar object that will convert the date and time value in milliseconds to date.
            Calendar calendar = Calendar.getInstance();
            calendar.setTimeInMillis(milliSeconds);
            return formatter.format(calendar.getTime());
        }

        private long getMillisOfDate(String datetime) {
            SimpleDateFormat formatter = new SimpleDateFormat("MMMM dd, yyyy");
            formatter.setLenient(false);

            Log.e("TAG", "getMillisOfDate: " + datetime);

            String oldTime = datetime;
            Date oldDate = null;
            try {
                oldDate = formatter.parse(oldTime);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            long oldMillis = oldDate.getTime();
            return oldMillis;
        }

    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }
}
