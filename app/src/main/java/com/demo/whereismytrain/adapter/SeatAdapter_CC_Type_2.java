package com.demo.whereismytrain.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.demo.whereismytrain.R;
import com.demo.whereismytrain.widgets.BaseModulesAdapter;

import java.util.ArrayList;

public class SeatAdapter_CC_Type_2 extends BaseModulesAdapter<SeatAdapter_CC_Type_2.ItemHolder> {

    Context context;
    ArrayList<String> seatList;

    public SeatAdapter_CC_Type_2(Context context, ArrayList<String> seatList) {
        this.context = context;
        this.seatList = seatList;
    }

    @Override
    public ItemHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == 0) {
            View v = LayoutInflater.from(context).inflate(R.layout.coach_cc_one_side_bottom_right, parent, false);
            return new ItemHolder(v);
        } else if (viewType == 1) {
            View v = LayoutInflater.from(context).inflate(R.layout.coach_cc_one_side_bottom, parent, false);
            return new ItemHolder(v);
        } else if (viewType == 2) {
            View v = LayoutInflater.from(context).inflate(R.layout.coach_cc_one_side_up_right, parent, false);
            return new ItemHolder(v);
        } else {
            View v = LayoutInflater.from(context).inflate(R.layout.coach_cc_one_side_bottom_right, parent, false);
            return new ItemHolder(v);
        }
    }

    @Override
    public void onBindViewHolder(ItemHolder holder, int position) {

    }

    @Override
    public int getItemViewType(int position) {
        if ((position == 0)) {
            return 0;
        } else if ((position == seatList.size() - 1)) {
            return 2;
        } else {
            return 1;
        }
    }

    @Override
    public int getItemCount() {
        return seatList.size();
    }

    public static class ItemHolder extends RecyclerView.ViewHolder {

        View blank_devider;
        CheckBox seat_checkbox;

        public ItemHolder(@NonNull View itemView) {
            super(itemView);
            seat_checkbox = itemView.findViewById(R.id.seat_checkbox);
            blank_devider = itemView.findViewById(R.id.blank_devider);
        }
    }

}
