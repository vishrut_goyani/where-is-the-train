package com.demo.whereismytrain.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.demo.whereismytrain.R;
import com.demo.whereismytrain.model.traindao.StationTrainsModel;
import com.demo.whereismytrain.widgets.BaseModulesAdapter;
import com.google.android.material.card.MaterialCardView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class StationTrainsAdapter extends BaseModulesAdapter<StationTrainsAdapter.ItemHolder> {

    Context context;
    ArrayList<StationTrainsModel.Trains_Details> trains_list;
    String formatedTimeSource;

    public StationTrainsAdapter(Context context, ArrayList<StationTrainsModel.Trains_Details> trains_list) {
        this.context = context;
        this.trains_list = trains_list;
    }

    @Override
    public ItemHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.item_station_trains, parent, false);
        return new ItemHolder(v);
    }

    @Override
    public void onBindViewHolder(ItemHolder holder, int position) {

        StationTrainsModel.Trains_Details trainsDetails = trains_list.get(position);

        holder.num_train.setText(trainsDetails.getTrainNo());
        holder.name_train.setText(trainsDetails.getTrainName());
        holder.textSourceName.setText(trainsDetails.getSource());
        holder.textDestName.setText(trainsDetails.getDestination());

        SimpleDateFormat sdf = new SimpleDateFormat("hh:mm:ss");
        try {
            Date dt = sdf.parse(trainsDetails.getArrivalTime());
            SimpleDateFormat sdfs = new SimpleDateFormat("hh:mm a");
            formatedTimeSource = sdfs.format(dt);
            holder.textArrivalTime.setText(formatedTimeSource);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        holder.cardMain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });

    }

    public static class ItemHolder extends RecyclerView.ViewHolder {

        TextView num_train, name_train, textSourceName, textDestName, textArrivalTime;
        MaterialCardView cardMain;

        public ItemHolder(@NonNull View itemView) {
            super(itemView);

            num_train = itemView.findViewById(R.id.num_train);
            name_train = itemView.findViewById(R.id.name_train);
            textSourceName = itemView.findViewById(R.id.textSourceName);
            textDestName = itemView.findViewById(R.id.textDestName);
            textArrivalTime = itemView.findViewById(R.id.textArrivalTime);
            cardMain = itemView.findViewById(R.id.cardMain);

        }

    }

    @Override
    public int getItemCount() {
        return trains_list.size();
    }

}
