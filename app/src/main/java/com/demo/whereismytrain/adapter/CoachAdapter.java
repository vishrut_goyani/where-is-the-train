package com.demo.whereismytrain.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.demo.whereismytrain.R;
import com.demo.whereismytrain.activity.CoachActivity;
import com.demo.whereismytrain.interfaces.OnItemClicked;
import com.demo.whereismytrain.model.traindao.CoachModel;
import com.demo.whereismytrain.widgets.BaseModulesAdapter;

import java.util.ArrayList;

public class CoachAdapter extends BaseModulesAdapter<CoachAdapter.ItemHolder> {

    Context context;
    ArrayList<CoachModel.Coach_Details> coachList;
    OnItemClicked onItemClicked;

    public CoachAdapter(Context context, ArrayList<CoachModel.Coach_Details> coachList, OnItemClicked onItemClicked) {
        this.context = context;
        this.coachList = coachList;
        this.onItemClicked = onItemClicked;
    }

    @Override
    public ItemHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == 0) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_engine, parent, false);
            return new ItemHolder(v);
        } else if (viewType == 1) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_coach, parent, false);
            return new ItemHolder(v);
        } else {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_coach, parent, false);
            return new ItemHolder(v);
        }
    }

    @Override
    public void onBindViewHolder(ItemHolder holder, int position) {

        if (holder.getItemViewType() == 0) {
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onItemClicked.onItemClick(position);
                }
            });
        } else {
            holder.tv_item.setText(coachList.get(position).getCode());
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onItemClicked.onItemClick(position);
                }
            });
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (coachList.get(position).getType() == CoachActivity.TYPE_ENGINE)
            return 0;
        else if (coachList.get(position).getType() == CoachActivity.TYPE_COACH)
            return 1;
        else
            return 1;
    }

    @Override
    public int getItemCount() {
        return coachList.size();
    }

    public static class ItemHolder extends RecyclerView.ViewHolder {

        TextView tv_item;

        public ItemHolder(@NonNull View itemView) {
            super(itemView);

            tv_item = itemView.findViewById(R.id.tv_item);
        }
    }

}
