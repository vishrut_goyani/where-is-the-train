package com.demo.whereismytrain.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.viewpager.widget.PagerAdapter;

import com.demo.whereismytrain.R;
import com.demo.whereismytrain.model.traindao.CoachModel;
import com.demo.whereismytrain.widgets.BaseRecyclerView;

import java.util.ArrayList;

public class ViewPagerCoachAdapter extends PagerAdapter {


    Context context;
    ArrayList<CoachModel.Coach_Details> coachList;
    BaseRecyclerView rv_seat_list;
    TextView textCoachCode;

    public ViewPagerCoachAdapter(Context context, ArrayList<CoachModel.Coach_Details> coachList) {
        this.context = context;
        this.coachList = coachList;
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        LayoutInflater inflater = LayoutInflater.from(context);
        ViewGroup layout = (ViewGroup) inflater.inflate(R.layout.item_coach_pager, container, false);

        init(layout);

        textCoachCode.setText(coachList.get(position).getCode());

        container.addView(layout);
        return layout;
    }

    private void init(ViewGroup layout) {
        rv_seat_list = layout.findViewById(R.id.rv_seat_list);
        textCoachCode = layout.findViewById(R.id.textCoachCode);
    }

    @Override
    public int getCount() {
        return coachList.size();
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == object;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object view) {
        container.removeView((RelativeLayout) view);
    }

    @Override
    public int getItemPosition(Object object) {
        return PagerAdapter.POSITION_NONE;
    }
}
