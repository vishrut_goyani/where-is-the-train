package com.demo.whereismytrain.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.demo.whereismytrain.R;
import com.demo.whereismytrain.activity.SearchStationsActivity;
import com.demo.whereismytrain.activity.StationStatusActivity;
import com.demo.whereismytrain.model.traindao.SearchStationModel;
import com.demo.whereismytrain.widgets.BaseModulesAdapter;

import java.util.ArrayList;
import java.util.Objects;

public class SearchStationAdapter extends BaseModulesAdapter<SearchStationAdapter.ItemHolder> {

    Context context;
    ArrayList<SearchStationModel> searchList;
    public static final String STATION_CODE = "station_code";
    public static final String STATION_NAME = "station_name";
    public static final String DEST_CODE = "destination_name";
    public static final String STATION_LAT = "station_latitude";
    public static final String STATION_LONG = "station_longitude";

    public SearchStationAdapter(Context context, ArrayList<SearchStationModel> searchList) {
        this.context = context;
        this.searchList = searchList;
    }

    @Override
    public ItemHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.item_search_station, parent, false);
        return new ItemHolder(v);
    }

    @Override
    public void onBindViewHolder(ItemHolder holder, int position) {

        if (position != 0) {
            holder.header_view.setVisibility(View.GONE);
        } else {
            holder.header_view.setVisibility(View.VISIBLE);
        }

        try {
            SearchStationModel searchByNoModel = searchList.get(position);
            holder.num_station.setText(searchByNoModel.getStation_code());
            holder.name_station.setText(searchByNoModel.getName_en());

            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (Objects.equals(((SearchStationsActivity) context)
                            .getIntent().getStringExtra(HomeModulesAdapter.MODULE_TYPE), "AfterStationSearchActivity")) {
                        Log.e("TAG", "onClick: " + "Clicked " + searchByNoModel.getName_en());
                        Log.e("TAG", "onTouch: " + "Clicked " + ((SearchStationsActivity) context)
                                .getIntent().getStringExtra(HomeModulesAdapter.MODULE_TYPE));
                        Intent intent = new Intent(context, StationStatusActivity.class)
                                .putExtra(STATION_NAME, searchByNoModel.getName_en())
                                .putExtra(STATION_CODE, searchByNoModel.getStation_code())
                                .putExtra(STATION_LAT, searchByNoModel.getLatitude())
                                .putExtra(STATION_LONG, searchByNoModel.getLongitude());
                        ((SearchStationsActivity) context).setResult(Activity.RESULT_OK, intent);
                        ((SearchStationsActivity) context).finish();
                    } else if (Objects.equals(((SearchStationsActivity) context)
                            .getIntent().getStringExtra(HomeModulesAdapter.MODULE_TYPE), "StationStatusActivity")) {
                        Intent intent = new Intent(context, StationStatusActivity.class)
                                .putExtra(STATION_NAME, searchByNoModel.getName_en())
                                .putExtra(STATION_CODE, searchByNoModel.getStation_code())
                                .putExtra(STATION_LAT, searchByNoModel.getLatitude())
                                .putExtra(STATION_LONG, searchByNoModel.getLongitude());
                        context.startActivity(intent);
                    }
                }
            });
        } catch (
                Exception e) {
            Log.e("TAG", "onBindViewHolder: " + e.getMessage());
        }

    }

    public static class ItemHolder extends RecyclerView.ViewHolder {

        View header_view;
        TextView num_station, name_station;

        public ItemHolder(@NonNull View itemView) {
            super(itemView);

            header_view = itemView.findViewById(R.id.header_view);
            num_station = itemView.findViewById(R.id.num_train);
            name_station = itemView.findViewById(R.id.name_train);

        }

    }

    public void updateSearchResults(ArrayList<SearchStationModel> searchResults) {
        this.searchList = searchResults;
    }


    @Override
    public int getItemCount() {
        Log.e("TAG", "getItemCount: " + searchList.size());
        return searchList.size();
    }

}
