package com.demo.whereismytrain.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.demo.whereismytrain.R;
import com.demo.whereismytrain.widgets.BaseModulesAdapter;

import java.util.ArrayList;

public class SeatAdapter_SL extends BaseModulesAdapter<SeatAdapter_SL.ItemHolder> {

    Context context;
    ArrayList<String> seatList;

    public SeatAdapter_SL(Context context, ArrayList<String> seatList) {
        this.context = context;
        this.seatList = seatList;
    }

    @Override
    public ItemHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == 0) {
            View v = LayoutInflater.from(context).inflate(R.layout.coach_two_a_type_a_one_side_bottom, parent, false);
            return new ItemHolder(v);
        } else if (viewType == 1) {
            View v = LayoutInflater.from(context).inflate(R.layout.coach_two_a_type_a_two_side, parent, false);
            return new ItemHolder(v);
        } else if (viewType == 2) {
            View v = LayoutInflater.from(context).inflate(R.layout.coach_two_a_type_a_one_side_up, parent, false);
            return new ItemHolder(v);
        } else {
            View v = LayoutInflater.from(context).inflate(R.layout.coach_two_a_type_a_one_side_bottom, parent, false);
            return new ItemHolder(v);
        }
    }

    @Override
    public void onBindViewHolder(ItemHolder holder, int position) {

    }

    @Override
    public int getItemViewType(int position) {
        if ((position == 0)) {
            return 0;
        } else if ((position == seatList.size() - 1)) {
            return 2;
        } else {
            return 1;
        }
    }

    @Override
    public int getItemCount() {
        return seatList.size();
    }

    public static class ItemHolder extends RecyclerView.ViewHolder {


        public ItemHolder(@NonNull View itemView) {
            super(itemView);

        }
    }

}
