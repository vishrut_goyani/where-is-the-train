package com.demo.whereismytrain.adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;
import androidx.viewpager.widget.PagerAdapter;

import com.demo.whereismytrain.R;
import com.demo.whereismytrain.model.ViewPagerModel;
import com.google.android.material.card.MaterialCardView;

import java.sql.Wrapper;
import java.util.ArrayList;
import java.util.List;

public class ViewPagerHomeAdapter extends PagerAdapter {

    Context context;
    ArrayList<ViewPagerModel> list;
    int[] colors;

    MaterialCardView cardPager;

    public ViewPagerHomeAdapter(Context context, ArrayList<ViewPagerModel> list) {
        this.context = context;
        this.list = list;
        colors = new int[]{
                context.getResources().getColor(R.color.colorLightOrange),
                context.getResources().getColor(R.color.colorLightPurple),
                context.getResources().getColor(R.color.colorLightGreen),
                context.getResources().getColor(R.color.colorLightYellow)
        };
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == object;
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        LayoutInflater inflater = LayoutInflater.from(context);
        ViewGroup layout = (ViewGroup) inflater.inflate(R.layout.item_home_viewpager, container, false);
        init(layout);

        cardPager.setCardBackgroundColor(colors[position]);
        container.addView(layout);

        return layout;
    }

    private void init(ViewGroup layout) {
        cardPager = layout.findViewById(R.id.cardPager);
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object view) {
        container.removeView((RelativeLayout) view);
    }

    @Override
    public int getItemPosition(Object object) {
        return PagerAdapter.POSITION_NONE;
    }

    /*private static class Container<T> {
        public static <T> List<T> getList(T... elements) {
            List<T> lst = new ArrayList<>();
            for (T element : elements) {
                lst.add(element);
            }
            return lst;
        }

    }*/
}
