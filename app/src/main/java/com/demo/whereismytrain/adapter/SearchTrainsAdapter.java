package com.demo.whereismytrain.adapter;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.demo.whereismytrain.R;
import com.demo.whereismytrain.activity.CoachActivity;
import com.demo.whereismytrain.activity.SearchTrainsActivity;
import com.demo.whereismytrain.activity.TrainsByNoActivity;
import com.demo.whereismytrain.model.traindao.SearchTrainModel;
import com.demo.whereismytrain.widgets.BaseModulesAdapter;

import java.util.ArrayList;
import java.util.Objects;

public class SearchTrainsAdapter extends BaseModulesAdapter<SearchTrainsAdapter.ItemHolder> {

    Context context;
    ArrayList<SearchTrainModel> searchList;
    private static final int TYPE_HEADER = 0;
    private static final int TYPE_ITEM = 1;
    public static final String TRAIN_CODE = "train_code";
    public static final String TRAIN_NAME = "train_name";
    public static final String TRAIN_SOURCE_CODE = "train_source_code";
    public static final String TRAIN_DESTINATION_CODE = "train_destination_code";
    public static final String SOURCE_NAME = "source_name";
    public static final String DESTINATION_NAME = "destination_name";
    public static final String TIME_DURATION_TIME = "train_duration";
    public static final String TRAIN_DESTINATION_TIME = "train_destination_time";

    public SearchTrainsAdapter(Context context, ArrayList<SearchTrainModel> searchList) {
        this.context = context;
        this.searchList = searchList;
    }

    @Override
    public ItemHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.item_search_by_no, parent, false);
        return new ItemHolder(v);
    }

    @Override
    public void onBindViewHolder(ItemHolder holder, int position) {

        if (position != 0) {
            holder.header_view.setVisibility(View.GONE);
        } else {
            holder.header_view.setVisibility(View.VISIBLE);
        }

        try {
            SearchTrainModel searchByNoModel = searchList.get(position);
            holder.num_train.setText(searchByNoModel.getTrain_no());
            holder.name_train.setText(searchByNoModel.getTrain_name());

            holder.stationsFrom.setText(searchByNoModel.getSource_name());
            holder.stationsTo.setText(searchByNoModel.getDestination_name());
            Log.e("TAG", "onBind: " + searchByNoModel.getTrain_name());

//            getStationName(searchByNoModel.getTrain_source(), holder.stationsFrom);
//            getStationName(searchByNoModel.getDestination_source(), holder.stationsTo);

            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Log.e("TAG", "onClick: " + "Clicked" + position);

                    if (Objects.equals(((SearchTrainsActivity) context)
                            .getIntent().getStringExtra(HomeModulesAdapter.MODULE_TYPE), "TrainsByNoActivity")) {
                        Intent intent = new Intent(context, TrainsByNoActivity.class)
                                .putExtra(TRAIN_CODE, searchByNoModel.getTrain_no())
                                .putExtra(TRAIN_NAME, searchByNoModel.getTrain_name())
                                .putExtra(TRAIN_SOURCE_CODE, searchByNoModel.getSource_code())
                                .putExtra(TRAIN_DESTINATION_CODE, searchByNoModel.getDestintion_code())
                                .putExtra(SOURCE_NAME, searchByNoModel.getSource_name())
                                .putExtra(DESTINATION_NAME, searchByNoModel.getDestination_name());
                        context.startActivity(intent);
                    } else if (Objects.equals(((SearchTrainsActivity) context)
                            .getIntent().getStringExtra(HomeModulesAdapter.MODULE_TYPE), "CoachActivity")) {
                        Intent intent = new Intent(context, CoachActivity.class)
                                .putExtra(TRAIN_CODE, searchByNoModel.getTrain_no());
                        context.startActivity(intent);
                    }
                }
            });
        } catch (Exception e) {
            Log.e("TAG", "onBindViewHolder: " + e.getMessage());
        }

    }

    public static class ItemHolder extends RecyclerView.ViewHolder {

        View header_view;
        TextView num_train, name_train, stationsFrom, stationsTo;

        public ItemHolder(@NonNull View itemView) {
            super(itemView);

            header_view = itemView.findViewById(R.id.header_view);
            num_train = itemView.findViewById(R.id.num_train);
            name_train = itemView.findViewById(R.id.name_train);
            stationsFrom = itemView.findViewById(R.id.stationsFrom);
            stationsTo = itemView.findViewById(R.id.stationsTo);

        }

    }

    public void updateSearchResults(ArrayList<SearchTrainModel> searchResults) {
        this.searchList = searchResults;
    }


    @Override
    public int getItemCount() {
        Log.e("TAG", "getItemCount: " + searchList.size());
        return searchList.size();
    }

    private boolean isPositionHeader(int position) {
        return position == 0;
    }

}
