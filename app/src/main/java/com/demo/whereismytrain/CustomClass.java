package com.demo.whereismytrain;

import android.util.Log;

import com.demo.whereismytrain.model.traindao.CancelledModel;
import com.demo.whereismytrain.model.traindao.DivertedModel;
import com.demo.whereismytrain.model.traindao.PartiallyCancelledModel;
import com.demo.whereismytrain.model.traindao.RescheduledModel;
import com.demo.whereismytrain.utils.TrainUtils;

import java.util.ArrayList;
import java.util.Objects;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class CustomClass<T> {

    public T type;

    public void setType(T type) {
        this.type = type;
    }

    public T getTypeName() {
        Log.e("TAG", "getTypeName: " + type.getClass());
        return type;
    }

    public T getType() {
        return type;
    }

    public Retrofit getTypeCancelled() {

        return new Retrofit.Builder()
                .baseUrl(TrainUtils.CANCELLED_TRAINS)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

    }

    public Retrofit getTypeDiverted() {

        return new Retrofit.Builder()
                .baseUrl(TrainUtils.DIVERTED_TRAINS)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

    }

    public Retrofit getTypeRescheduled() {

        return new Retrofit.Builder()
                .baseUrl(TrainUtils.RESCHEDULED_TRAINS)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

    }

    public Retrofit getTypePartiallyCancelled() {

        return new Retrofit.Builder()
                .baseUrl(TrainUtils.PARTIALLY_CANCELLED_TRAINS)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

    }



    /*private static class Container<T> {
        public static <T> List<T> getList(T... elements) {
            List<T> lst = new ArrayList<>();
            for (T element : elements) {
                lst.add(element);
            }
            return lst;
        }

    }*/

}
