package com.demo.whereismytrain.widgets;

import android.content.Context;

import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;

public class CustomLinearLayoutManager extends GridLayoutManager {

    private boolean isScrollEnabled = true;

    public CustomLinearLayoutManager(Context context, int span) {
        super(context, span);
    }

    public void setScrollEnabled(boolean flag) {
        this.isScrollEnabled = flag;
    }

    @Override
    public boolean canScrollVertically() {
        return isScrollEnabled && super.canScrollVertically();
    }

}
