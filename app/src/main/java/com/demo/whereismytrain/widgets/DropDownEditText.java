package com.demo.whereismytrain.widgets;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;

import androidx.appcompat.widget.AppCompatEditText;

public class DropDownEditText extends AppCompatEditText {
    final int DRAWABLE_LEFT = 0;
    final int DRAWABLE_TOP = 1;
    final int DRAWABLE_RIGHT = 2;
    final int DRAWABLE_BOTTOM = 3;

    public DropDownEditText(Context context) {
        super(context);
    }

    public DropDownEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public interface OnIconClickListener {
        void onRightIconClick();

        void onLeftIconClick();
    }

    private OnIconClickListener onIconClickListener;

    public void setOnIconClickListener(OnIconClickListener onIconClickListener) {
        this.onIconClickListener = onIconClickListener;
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {

        if (event.getAction() == MotionEvent.ACTION_UP) {
            Drawable drawableRight = getCompoundDrawables()[DRAWABLE_RIGHT];
            Drawable drawableLeft = getCompoundDrawables()[DRAWABLE_LEFT];
            if (drawableRight != null) {
                //The x-axis coordinates of this click event, if > current control width - control right spacing - drawable actual display size
                if (event.getX() >= (getWidth() - getPaddingRight() - drawableRight.getIntrinsicWidth())) {
                    //Set up to click the EditText icon on the right to lose focus.
                    // Prevent clicking EditText icon on the right side of EditText to get focus and pop-up the soft keyboard
                    Log.e("TAG", "onTouchEvent: " + "Right Icon");
                    setFocusableInTouchMode(false);
                    setFocusable(false);
                    if (onIconClickListener != null) {
                        onIconClickListener.onRightIconClick();
                    }
                } else if (event.getX() <= (getPaddingLeft() + drawableLeft.getIntrinsicWidth())) {
                    //Set up to click the EditText icon on the right to lose focus.
                    // Prevent clicking EditText icon on the right side of EditText to get focus and pop-up the soft keyboard
                    Log.e("TAG", "onTouchEvent: " + "Left Icon");
                    setFocusableInTouchMode(false);
                    setFocusable(false);
                    if (onIconClickListener != null) {
                        onIconClickListener.onLeftIconClick();
                    }
                } else {
                    setFocusableInTouchMode(true);
                    setFocusable(true);
                }
            }
        }
        return super.onTouchEvent(event);
    }
}