package com.demo.whereismytrain.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.demo.whereismytrain.CustomClass;
import com.demo.whereismytrain.R;
import com.demo.whereismytrain.adapter.CancelledTrainsAdapter;
import com.demo.whereismytrain.adapter.HomeModulesAdapter;
import com.demo.whereismytrain.interfaces.APIService;
import com.demo.whereismytrain.model.traindao.CancelledModel;
import com.demo.whereismytrain.model.traindao.DivertedModel;
import com.demo.whereismytrain.model.traindao.PartiallyCancelledModel;
import com.demo.whereismytrain.model.traindao.RescheduledModel;
import com.demo.whereismytrain.utils.TrainUtils;
import com.demo.whereismytrain.widgets.BaseRecyclerView;

import java.util.ArrayList;
import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class OtherTrainsActivity extends AppCompatActivity {

    BaseRecyclerView rv_train_list;
    ProgressDialog progressDialog;
    ArrayList<CancelledModel.Trains_Details> cancelledArrayList;
    ArrayList<DivertedModel.Trains_Details> divertedArrayList;
    ArrayList<PartiallyCancelledModel.Trains_Details> partiallycancelledArrayList;
    ArrayList<RescheduledModel.Trains_Details> recheduledArrayList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_other_trains);
        progressDialog = new ProgressDialog(this);
        init();

    }


    private void init() {
        cancelledArrayList = new ArrayList<>();
        divertedArrayList = new ArrayList<>();
        partiallycancelledArrayList = new ArrayList<>();
        recheduledArrayList = new ArrayList<>();
        rv_train_list = findViewById(R.id.rv_train_list);
        rv_train_list.setLayoutManager(new LinearLayoutManager(this));
        rv_train_list.setHasFixedSize(true);
        rv_train_list.setEmptyView(this, findViewById(R.id.empty_view), getResources().getDrawable(R.drawable.ic_train), "No Trains found");

        getAllCustomTrains(getIntent().getStringExtra(HomeModulesAdapter.CHOSEN_DATE));

    }

    private void getAllCustomTrains(String date) {
        progressDialog.setCancelable(false);
        progressDialog.setTitle("Loading...");
        progressDialog.setMessage("Fetching data from server...");
        progressDialog.show();

        if (getIntent().getStringExtra(HomeModulesAdapter.TRAIN_STATUS_TYPE).equals("cancelled")) {
            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(TrainUtils.CANCELLED_TRAINS)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();

            APIService service = retrofit.create(APIService.class);
            Call<CancelledModel> call = service.getCancelledTrains(date);

            call.enqueue(new Callback<CancelledModel>() {
                @Override
                public void onResponse(Call<CancelledModel> call, Response<CancelledModel> response) {
                    progressDialog.dismiss();
                    CancelledModel cancelledModel = response.body();
                    if (Objects.equals(cancelledModel.getResponseCode(), "200")) {
                        cancelledArrayList.clear();
                        cancelledArrayList = new ArrayList<>();

                        cancelledArrayList = cancelledModel.getTrains();
                        CancelledTrainsAdapter cancelledTrainsAdapter = new CancelledTrainsAdapter(OtherTrainsActivity.this, cancelledArrayList);
                        rv_train_list.setAdapter(cancelledTrainsAdapter);
                    } else {
                        Toast.makeText(OtherTrainsActivity.this, response.message(), Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<CancelledModel> call, Throwable t) {
                    progressDialog.dismiss();
                    Log.e("TAG", "onFailure: " + t.getMessage());
                }
            });
        } else if (getIntent().getStringExtra(HomeModulesAdapter.TRAIN_STATUS_TYPE).equals("diverted")) {
            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(TrainUtils.DIVERTED_TRAINS)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();

            APIService service = retrofit.create(APIService.class);
            Call<DivertedModel> call = service.getDivertedTrains(date);

            call.enqueue(new Callback<DivertedModel>() {
                @Override
                public void onResponse(Call<DivertedModel> call, Response<DivertedModel> response) {
                    progressDialog.dismiss();
                    DivertedModel divertedModel = response.body();
                    if (Objects.equals(divertedModel.getResponseCode(), "200")) {
                        divertedArrayList.clear();
                        divertedArrayList = new ArrayList<>();

                        divertedArrayList = divertedModel.getTrains();
                        CancelledTrainsAdapter cancelledTrainsAdapter = new CancelledTrainsAdapter(OtherTrainsActivity.this, cancelledArrayList);
                        rv_train_list.setAdapter(cancelledTrainsAdapter);
                    } else {
                        Toast.makeText(OtherTrainsActivity.this, response.message(), Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<DivertedModel> call, Throwable t) {
                    progressDialog.dismiss();
                    Log.e("TAG", "onFailure: " + t.getMessage());
                }
            });
        } else if (getIntent().getStringExtra(HomeModulesAdapter.TRAIN_STATUS_TYPE).equals("partially_cancelled")) {
            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(TrainUtils.PARTIALLY_CANCELLED_TRAINS)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();

            APIService service = retrofit.create(APIService.class);

            Call<PartiallyCancelledModel> call = service.getPartiallyCancelledCustomTrains(date);

            call.enqueue(new Callback<PartiallyCancelledModel>() {
                @Override
                public void onResponse(Call<PartiallyCancelledModel> call, Response<PartiallyCancelledModel> response) {
                    progressDialog.dismiss();
                    PartiallyCancelledModel partiallyCancelledModel = response.body();
                    if (Objects.equals(partiallyCancelledModel.getResponseCode(), "200")) {
                        partiallycancelledArrayList.clear();
                        partiallycancelledArrayList = new ArrayList<>();

                        partiallycancelledArrayList = partiallyCancelledModel.getTrains();
                        CancelledTrainsAdapter cancelledTrainsAdapter = new CancelledTrainsAdapter(OtherTrainsActivity.this, cancelledArrayList);
                        rv_train_list.setAdapter(cancelledTrainsAdapter);
                    } else {
                        Toast.makeText(OtherTrainsActivity.this, response.message(), Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<PartiallyCancelledModel> call, Throwable t) {
                    progressDialog.dismiss();
                    Log.e("TAG", "onFailure: " + t.getMessage());
                }
            });
        } else if (getIntent().getStringExtra(HomeModulesAdapter.TRAIN_STATUS_TYPE).equals("rescheduled")) {
            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(TrainUtils.RESCHEDULED_TRAINS)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();

            APIService service = retrofit.create(APIService.class);

            Call<RescheduledModel> call = service.getRescheduledTrains(date);

            call.enqueue(new Callback<RescheduledModel>() {
                @Override
                public void onResponse(Call<RescheduledModel> call, Response<RescheduledModel> response) {
                    progressDialog.dismiss();
                    RescheduledModel rescheduledModel = response.body();
                    if (Objects.equals(rescheduledModel.getResponseCode(), "200")) {
                        recheduledArrayList.clear();
                        recheduledArrayList = new ArrayList<>();

                        recheduledArrayList = rescheduledModel.getTrains();
                        CancelledTrainsAdapter cancelledTrainsAdapter = new CancelledTrainsAdapter(OtherTrainsActivity.this, cancelledArrayList);
                        rv_train_list.setAdapter(cancelledTrainsAdapter);
                    } else {
                        Toast.makeText(OtherTrainsActivity.this, response.message(), Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<RescheduledModel> call, Throwable t) {
                    progressDialog.dismiss();
                    Log.e("TAG", "onFailure: " + t.getMessage());
                }
            });
        }

    }


    private void getAllCancelledTrains(String date) {
        progressDialog.setCancelable(false);
        progressDialog.setTitle("Loading...");
        progressDialog.setMessage("Fetching data from server...");
        progressDialog.show();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(TrainUtils.CANCELLED_TRAINS)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        APIService service = retrofit.create(APIService.class);
        Call<CancelledModel> call = service.getCancelledTrains(date);

        call.enqueue(new Callback<CancelledModel>() {
            @Override
            public void onResponse(Call<CancelledModel> call, Response<CancelledModel> response) {
                progressDialog.dismiss();
                if (Objects.equals(response.body().getResponseCode(), "200")) {
                    cancelledArrayList.clear();
                    cancelledArrayList = new ArrayList<>();

                    cancelledArrayList = response.body().getTrains();
                    CancelledTrainsAdapter cancelledTrainsAdapter = new CancelledTrainsAdapter(OtherTrainsActivity.this, cancelledArrayList);
                    rv_train_list.setAdapter(cancelledTrainsAdapter);
                } else {
                    Toast.makeText(OtherTrainsActivity.this, response.message(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<CancelledModel> call, Throwable t) {
                progressDialog.dismiss();
                Log.e("TAG", "onFailure: " + t.getMessage());
            }
        });
    }
}