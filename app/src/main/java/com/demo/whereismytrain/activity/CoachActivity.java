package com.demo.whereismytrain.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.FileProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.LinearSnapHelper;
import androidx.viewpager.widget.ViewPager;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;

import com.demo.whereismytrain.R;
import com.demo.whereismytrain.adapter.CoachAdapter;
import com.demo.whereismytrain.adapter.SearchTrainsAdapter;
import com.demo.whereismytrain.adapter.SeatAdapter_1A_Type_A;
import com.demo.whereismytrain.adapter.ViewPagerCoachAdapter;
import com.demo.whereismytrain.interfaces.APIService;
import com.demo.whereismytrain.interfaces.OnItemClicked;
import com.demo.whereismytrain.model.traindao.CoachModel;
import com.demo.whereismytrain.utils.ScreenUtils;
import com.demo.whereismytrain.utils.TrainUtils;
import com.demo.whereismytrain.widgets.BaseRecyclerView;
import com.demo.whereismytrain.widgets.SliderLayoutManager;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class CoachActivity extends AppCompatActivity {

    BaseRecyclerView rvHorizontalPicker, rv_seat_picker;
    ViewPagerCoachAdapter viewPagerCoachAdapter;
    SeatAdapter_1A_Type_A seatAdapter1ATypeA;
    ArrayList<CoachModel.Coach_Details> coachList;
    ArrayList<String> seatList;
    public static final int TYPE_ENGINE = 0;
    public static final int TYPE_COACH = 1;
    ViewPager viewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_coach);
        init();
    }

    private void init() {
        seatList = new ArrayList<>();
        coachList = new ArrayList<>();
        rvHorizontalPicker = findViewById(R.id.rv_horizontal_picker);
        viewPager = findViewById(R.id.viewPager);
        int padding = ScreenUtils.getScreenWidth(this) / 2 - ScreenUtils.dpToPx(this, 32);
        rvHorizontalPicker.setPadding(padding, 0, padding, 0);
        SliderLayoutManager sliderLayoutManager = new SliderLayoutManager(this);
        sliderLayoutManager.setCallback(new SliderLayoutManager.OnItemSelectedListener() {
            @Override
            public void onItemSelected(int layoutPosition) {
                Log.e("TAG", "onItemSelected: " + (layoutPosition + 1));
            }
        });
        rvHorizontalPicker.setLayoutManager(sliderLayoutManager);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        new LinearSnapHelper().attachToRecyclerView(rvHorizontalPicker);

        for (int i = 0; i < 8; i++) {
            seatList.add("" + i);
        }

        getCoachList(getIntent().getStringExtra(SearchTrainsAdapter.TRAIN_CODE));
//        ReadFile();

    }

    public void ReadFile() {
        Log.e("TAG", "Sample: ");
        try {

            File file = new File(Environment.getExternalStorageDirectory().toString());
            File jsonFile = new File(file, "python.json");

            Uri uri = FileProvider.getUriForFile(this, getPackageName() + ".myprovider", jsonFile);
            grantUriPermission(getPackageName(), uri, Intent.FLAG_GRANT_READ_URI_PERMISSION);

            FileInputStream stream = new FileInputStream(jsonFile);
            try {
                FileChannel fc = stream.getChannel();
                MappedByteBuffer bb = fc.map(FileChannel.MapMode.READ_ONLY, 0, fc.size());

                String jsonStr = Charset.defaultCharset().decode(bb).toString();

                JSONObject jsonObj = new JSONObject(jsonStr);

                // Getting data JSON Array nodes
                JSONArray data = jsonObj.getJSONArray("images");
                // Getting data JSON Array nodes

                Log.e("TAG", "ReadFile: " + data.length());

                // looping through All nodes
                for (int i = 0; i < data.length(); i++) {
                    JSONObject c = data.getJSONObject(i);

                    String name = c.getString("name");
                    String w = c.getString("w");
                    String h = c.getString("h");

                    //use >  int id = c.getInt("duration"); if you want get an int


                    // tmp hashmap for single node
                    HashMap<String, String> parsedData = new HashMap<String, String>();

                    // adding each child node to HashMap key => value
                    parsedData.put("name", name);
                    parsedData.put("w", w);
                    parsedData.put("h", h);

                    Log.e("TAG", "ReadFile: " + parsedData.get("name") + "\n" + parsedData.get("w") + "\n" + parsedData.get("h"));

                    // do what do you want on your interface
                }

            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                stream.close();
            }
            /*  String jsonStr = "{\n\"data\": [\n    {\n        \"id\": \"1\",\n        \"title\": \"Farhan Shah\",\n        \"duration\": 10\n    },\n    {\n        \"id\": \"2\",\n        \"title\": \"Noman Shah\",\n        \"duration\": 10\n    },\n    {\n        \"id\": \"3\",\n        \"title\": \"Ahmad Shah\",\n        \"duration\": 10\n    },\n    {\n        \"id\": \"4\",\n        \"title\": \"Mohsin Shah\",\n        \"duration\": 10\n    },\n    {\n        \"id\": \"5\",\n        \"title\": \"Haris Shah\",\n        \"duration\": 10\n    }\n  ]\n\n}\n";
             */

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void getCoachList(String train_no) {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(TrainUtils.TRAIN_COACH)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        APIService service = retrofit.create(APIService.class);
        Call<CoachModel> call = service.getCoachInfo(train_no);
        call.enqueue(new Callback<CoachModel>() {
            @Override
            public void onResponse(Call<CoachModel> call, Response<CoachModel> response) {
                if (Objects.equals(response.body().getResponseCode(), "200")) {
                    coachList.clear();
                    coachList = new ArrayList<>();
                    for (int i = 0; i < response.body().getCoaches().size(); i++) {
                        CoachModel.Coach_Details coach_details;
                        coach_details = response.body().getCoaches().get(i);
                        if (i == 0)
                            coach_details.setType(TYPE_ENGINE);
                        else
                            coach_details.setType(TYPE_COACH);
                        coachList.add(coach_details);
                    }
                    CoachAdapter coachAdapter = new CoachAdapter(CoachActivity.this, coachList, new OnItemClicked() {
                        @Override
                        public void onItemClick(int position) {
                            rvHorizontalPicker.smoothScrollToPosition(position);
                        }
                    });
                    rvHorizontalPicker.setAdapter(coachAdapter);

                    viewPagerCoachAdapter = new ViewPagerCoachAdapter(CoachActivity.this, coachList);
                    viewPager.setAdapter(viewPagerCoachAdapter);

                } else {
                    Log.e("TAG", "onResponse: " + response.message());
                }
            }

            @Override
            public void onFailure(Call<CoachModel> call, Throwable t) {
                Log.e("TAG", "onFailure: " + t.getMessage());
            }
        });
    }

}