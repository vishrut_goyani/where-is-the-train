package com.demo.whereismytrain.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.animation.Animator;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.ConnectivityManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import com.demo.whereismytrain.R;
import com.demo.whereismytrain.TrainApplication;
import com.demo.whereismytrain.adapter.SearchTrainsAdapter;
import com.demo.whereismytrain.database.DatabaseHelper;
import com.demo.whereismytrain.interfaces.APIService;
import com.demo.whereismytrain.model.traindao.AllTrainModel;
import com.demo.whereismytrain.model.traindao.SearchByNoModel;
import com.demo.whereismytrain.model.traindao.SearchTrainModel;
import com.demo.whereismytrain.widgets.HidingScrollListener;
import com.demo.whereismytrain.utils.TrainUtils;
import com.demo.whereismytrain.widgets.BaseRecyclerView;
import com.paulrybitskyi.persistentsearchview.PersistentSearchView;
import com.paulrybitskyi.persistentsearchview.listeners.OnSearchConfirmedListener;
import com.paulrybitskyi.persistentsearchview.listeners.OnSearchQueryChangeListener;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.demo.whereismytrain.database.DatabaseHelper.COL_DESTINATION_CODE;
import static com.demo.whereismytrain.database.DatabaseHelper.COL_DESTINATION_NAME;
import static com.demo.whereismytrain.database.DatabaseHelper.COL_SOURCE_CODE;
import static com.demo.whereismytrain.database.DatabaseHelper.COL_SOURCE_NAME;
import static com.demo.whereismytrain.database.DatabaseHelper.COL_TRAIN_NAME;
import static com.demo.whereismytrain.database.DatabaseHelper.COL_TRAIN_NO;
import static com.demo.whereismytrain.database.DatabaseHelper.TRAIN_LIST_TABLE;

public class SearchTrainsActivity extends AppCompatActivity {

    SearchTrainsAdapter searchTrainsAdapter;
    private InputMethodManager mImm;
    private BaseRecyclerView recyclerView;
    private ArrayList<SearchTrainModel> searchList;
    ArrayList<AllTrainModel.Trains_Data> all_trains_list;
    PersistentSearchView persistentSearchView;
    DatabaseHelper helper;
    SQLiteDatabase database;

    ProgressDialog progressDialog;
    SharedPreferences preferences;
    SharedPreferences.Editor editor;
    public static final String downloadedAllTrains = "downloadedTrains";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        progressDialog = new ProgressDialog(this);
        helper = new DatabaseHelper(this);
        database = helper.getWritableDatabase();
        preferences = getSharedPreferences(TrainApplication.APP_PREFS, MODE_PRIVATE);
        editor = preferences.edit();
        init();

    }

    private void init() {
        searchList = new ArrayList<>();
        all_trains_list = new ArrayList<>();
        mImm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);

        persistentSearchView = findViewById(R.id.persistentSearchView);
        recyclerView = findViewById(R.id.recyclerview);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.addOnScrollListener(new HidingScrollListener() {
            @Override
            public void onHide() {
                hideViews();
            }

            @Override
            public void onShow() {
                showViews();
            }
        });

        if (!preferences.getBoolean(downloadedAllTrains, false))
            getAllTrains();

        initSearch();

    }

    private void initSearch() {
        persistentSearchView.requestFocus();
        persistentSearchView.setOnLeftBtnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        persistentSearchView.setOnSearchConfirmedListener(new OnSearchConfirmedListener() {
            @Override
            public void onSearchConfirmed(PersistentSearchView searchView, String query) {
                hideInputManager();
            }
        });
        persistentSearchView.setOnSearchQueryChangeListener(new OnSearchQueryChangeListener() {
            @Override
            public void onSearchQueryChanged(PersistentSearchView searchView, String oldQuery, String newQuery) {
                if (!TextUtils.isEmpty(newQuery)) {
                    getTrainsOffline(newQuery);
                }
            }
        });
    }

    private void getTrainsOffline(String newQuery) {

        searchList.clear();
        searchList = new ArrayList<>();


        Cursor cursor = database.rawQuery(" SELECT * FROM " + TRAIN_LIST_TABLE +
                " WHERE " + COL_TRAIN_NO + " LIKE '" + newQuery + "%' OR "
                + COL_TRAIN_NAME + " LIKE '" + newQuery + "%'", null);

        if (cursor != null) {
            if (cursor.moveToFirst()) {
                do {
                    String train_no = cursor.getString(cursor.getColumnIndex(DatabaseHelper.COL_TRAIN_NO));
                    String train_name = cursor.getString(cursor.getColumnIndex(DatabaseHelper.COL_TRAIN_NAME));
                    String source_code = cursor.getString(cursor.getColumnIndex(DatabaseHelper.COL_SOURCE_CODE));
                    String destination_code = cursor.getString(cursor.getColumnIndex(DatabaseHelper.COL_DESTINATION_CODE));
                    String source_name = cursor.getString(cursor.getColumnIndex(DatabaseHelper.COL_SOURCE_NAME));
                    String destination_name = cursor.getString(cursor.getColumnIndex(DatabaseHelper.COL_DESTINATION_NAME));

                    SearchTrainModel searchTrainModel = new SearchTrainModel(
                            train_no,
                            train_name,
                            source_code,
                            destination_code,
                            source_name,
                            destination_name
                    );
                    searchList.add(searchTrainModel);
                } while (cursor.moveToNext());
            }

            searchTrainsAdapter = new SearchTrainsAdapter(this, searchList);
            recyclerView.setAdapter(searchTrainsAdapter);
            cursor.close();
        }

    }

    private void getAllTrains() {
        progressDialog.setCancelable(false);
        progressDialog.setTitle("Loading...");
        progressDialog.setMessage("Fetching data from server...");
        progressDialog.show();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(TrainUtils.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        APIService service = retrofit.create(APIService.class);
        Call<AllTrainModel> call = service.getAllTrains();

        call.enqueue(new Callback<AllTrainModel>() {
            @Override
            public void onResponse(Call<AllTrainModel> call, Response<AllTrainModel> response) {
                progressDialog.dismiss();
                if (response.body().getStatus()) {
                    all_trains_list = response.body().getData();
                    Log.e("TAG", "SourceName: " + all_trains_list.get(0).getDestination());
                    if (all_trains_list.size() > 0)
                        new SaveOfflineTask().execute(all_trains_list);
                } else {
                    Log.e("TAG", "onResponse: " + response.message());
                }
            }

            @Override
            public void onFailure(Call<AllTrainModel> call, Throwable t) {
                progressDialog.dismiss();
                Log.e("TAG", "onFailure: " + t.getMessage());
            }
        });
    }

    private class SaveOfflineTask extends AsyncTask<ArrayList<AllTrainModel.Trains_Data>, Void, Void> {

        @Override
        protected Void doInBackground(ArrayList<AllTrainModel.Trains_Data>... arrayLists) {
            Log.e("TAG", "doInBackground: " + arrayLists[0].size());
            SQLiteDatabase db = helper.getWritableDatabase();
            db.beginTransaction();
            for (int i = 0; i < arrayLists[0].size(); i++) {

                ContentValues contentValues = new ContentValues();
                contentValues.put(COL_TRAIN_NO, all_trains_list.get(i).getTrain_no());
                contentValues.put(COL_TRAIN_NAME, all_trains_list.get(i).getTrain_name());
                contentValues.put(COL_SOURCE_CODE, all_trains_list.get(i).getSource());
                contentValues.put(COL_DESTINATION_CODE, all_trains_list.get(i).getDestination());
                contentValues.put(COL_SOURCE_NAME, all_trains_list.get(i).getSource_station_name());
                contentValues.put(COL_DESTINATION_NAME, all_trains_list.get(i).getDestination_train_station());
                db.insert(TRAIN_LIST_TABLE, null, contentValues);

            }
            db.setTransactionSuccessful();
            db.endTransaction();
            return null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog.setCancelable(false);
            progressDialog.setTitle("Loading...");
            progressDialog.setMessage("Saving data...");
            progressDialog.show();
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            editor.putBoolean(downloadedAllTrains, true);
            editor.apply();
            progressDialog.dismiss();
            persistentSearchView.requestFocus();
        }
    }

    private void hideViews() {
        persistentSearchView.animate().translationY(-persistentSearchView.getHeight()).setInterpolator(new AccelerateInterpolator(2));
    }

    private void showViews() {
        persistentSearchView.animate().translationY(0).setInterpolator(new DecelerateInterpolator(2f)).setListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {

            }

            @Override
            public void onAnimationEnd(Animator animation) {

            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });
    }

    public void hideInputManager() {
        if (persistentSearchView != null) {
            if (mImm != null) {
                mImm.hideSoftInputFromWindow(persistentSearchView.getWindowToken(), 0);
            }
            persistentSearchView.clearFocus();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        return cm.getActiveNetworkInfo() != null && cm.getActiveNetworkInfo().isConnected();
    }
}