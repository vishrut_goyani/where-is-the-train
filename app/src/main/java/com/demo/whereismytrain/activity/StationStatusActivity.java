package com.demo.whereismytrain.activity;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.demo.whereismytrain.R;
import com.demo.whereismytrain.adapter.HomeModulesAdapter;
import com.demo.whereismytrain.adapter.SearchStationAdapter;
import com.demo.whereismytrain.adapter.StationTrainsAdapter;
import com.demo.whereismytrain.interfaces.APIService;
import com.demo.whereismytrain.model.traindao.StationTrainsModel;
import com.demo.whereismytrain.utils.TrainUtils;
import com.demo.whereismytrain.widgets.BaseRecyclerView;
import com.google.android.material.card.MaterialCardView;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class StationStatusActivity extends AppCompatActivity {

    Toolbar toolbar;
    BaseRecyclerView recyclerview;
    ArrayList<StationTrainsModel.Trains_Details> trains_list;
    StationTrainsAdapter stationTrainsAdapter;
    TextView txtFrom, txtTo;
    MaterialCardView switch_card;
    LinearLayout ll_from;
    public static final int STATION_SEARCH_REQUEST_CODE = 101;
    AppCompatImageView locate_on_map;
    String uri;
    String latitude, longitude;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_station_status);

        init();
        getTrainList(getIntent().getStringExtra(SearchStationAdapter.STATION_CODE));

    }

    private void init() {
        toolbar = findViewById(R.id.toolbar);
        txtFrom = findViewById(R.id.txtFrom);
        txtTo = findViewById(R.id.txtTo);
        ll_from = findViewById(R.id.ll_from);
        recyclerview = findViewById(R.id.recyclerview);
        locate_on_map = findViewById(R.id.locate_on_map);
        switch_card = findViewById(R.id.switch_card);
        trains_list = new ArrayList<>();

        latitude = getIntent().getStringExtra(SearchStationAdapter.STATION_LAT);
        longitude = getIntent().getStringExtra(SearchStationAdapter.STATION_LONG);
        txtFrom.setText(getIntent().getStringExtra(SearchStationAdapter.STATION_NAME));

        if (!TextUtils.isEmpty(latitude) && !TextUtils.isEmpty(longitude))
            uri = "https://maps.google.com/maps?q=" + latitude + "," + longitude;

        switch_card.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });

        ll_from.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivityForResult(new Intent(StationStatusActivity.this,
                                SearchStationsActivity.class)
                                .putExtra(HomeModulesAdapter.MODULE_TYPE, "AfterStationSearchActivity"),
                        STATION_SEARCH_REQUEST_CODE);
            }
        });

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        locate_on_map.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
                startActivity(intent);
            }
        });

        recyclerview.setLayoutManager(new LinearLayoutManager(this));
        recyclerview.setHasFixedSize(true);

    }

    private void getTrainList(String str) {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(TrainUtils.STATION_TRAINS_LIST)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        APIService service = retrofit.create(APIService.class);

        Call<StationTrainsModel> call = service.getStationTrainsList(str);

        call.enqueue(new Callback<StationTrainsModel>() {
            @Override
            public void onResponse(Call<StationTrainsModel> call, Response<StationTrainsModel> response) {
                if (response.body().getStatus().equals("SUCCESS")) {
                    trains_list.clear();
                    trains_list = new ArrayList<>();
                    trains_list = response.body().getTrains();
                    stationTrainsAdapter = new StationTrainsAdapter(StationStatusActivity.this, trains_list);
                    recyclerview.setAdapter(stationTrainsAdapter);

                } else {
                    Toast.makeText(StationStatusActivity.this, response.message(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<StationTrainsModel> call, Throwable t) {
                Log.e("TAG", "onFailureTrain : " + t.getMessage());
            }
        });
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case STATION_SEARCH_REQUEST_CODE:
                if (resultCode == RESULT_OK) {
                    Log.e("TAG", "onActivityResult: " + data.getStringExtra(SearchStationAdapter.STATION_CODE));
                    String stationCode = data.getStringExtra(SearchStationAdapter.STATION_CODE);
                    String stationName = data.getStringExtra(SearchStationAdapter.STATION_NAME);
                    String latitude = data.getStringExtra(SearchStationAdapter.STATION_LAT);
                    String longitude = data.getStringExtra(SearchStationAdapter.STATION_LONG);
                    txtFrom.setText(stationName);
                    getTrainList(stationCode);
                    if (!TextUtils.isEmpty(latitude) && !TextUtils.isEmpty(longitude))
                        uri = "https://maps.google.com/maps?q=" + latitude + "," + longitude;
                }
                break;
        }
    }
}