package com.demo.whereismytrain.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.demo.whereismytrain.R;
import com.demo.whereismytrain.adapter.SearchTrainsAdapter;
import com.demo.whereismytrain.interfaces.APIService;
import com.demo.whereismytrain.model.traindao.AllStationModel;
import com.demo.whereismytrain.utils.TrainUtils;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.card.MaterialCardView;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class TrainsByNoActivity extends AppCompatActivity implements View.OnClickListener {

    AppBarLayout.OnOffsetChangedListener onOffsetChangedListener;
    TextView textSourceTime, textSourceName, textDestTime, textDestName, title_text;
    MaterialButton textSourceCode, textDestCode;
    Toolbar toolbar;
    TextView distance, duration;
    MaterialCardView running_card, route_card, seat_card, coach_card;
    double latitude1 = 0.0, longitude1 = 0.0, latitude2 = 0.0, longitude2 = 0.0;
    String formatedTimeSource, formatedTimeDest, timeDuration;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_trains_by_no);
        init();
        bindText();

        running_card.setOnClickListener(this);
        route_card.setOnClickListener(this);
        seat_card.setOnClickListener(this);
        coach_card.setOnClickListener(this);
    }

    private void bindText() {
        textSourceCode.setText(getIntent().getStringExtra(SearchTrainsAdapter.TRAIN_SOURCE_CODE));
        title_text.setText(getIntent().getStringExtra(SearchTrainsAdapter.TRAIN_NAME));
        textDestCode.setText(getIntent().getStringExtra(SearchTrainsAdapter.TRAIN_DESTINATION_CODE));
        textSourceName.setText(getIntent().getStringExtra(SearchTrainsAdapter.SOURCE_NAME));
        textDestName.setText(getIntent().getStringExtra(SearchTrainsAdapter.DESTINATION_NAME));
//        getStationNameSource(getIntent().getStringExtra(SearchTrainsAdapter.TRAIN_SOURCE), textSourceName);
//        getStationNameDestination(getIntent().getStringExtra(SearchTrainsAdapter.TRAIN_DESTINATION), textDestName);

    }

    private void init() {
        toolbar = findViewById(R.id.toolbar);
        running_card = findViewById(R.id.running_card);
        route_card = findViewById(R.id.route_card);
        seat_card = findViewById(R.id.seat_card);
        coach_card = findViewById(R.id.coach_card);
        title_text = findViewById(R.id.title_text);
        distance = findViewById(R.id.txtDistance);
        duration = findViewById(R.id.txtDuration);
        textSourceCode = findViewById(R.id.textSourceCode);
        textSourceTime = findViewById(R.id.textSourceTime);
        textSourceName = findViewById(R.id.textSourceName);
        textDestCode = findViewById(R.id.textDestCode);
        textDestTime = findViewById(R.id.textDestTime);
        textDestName = findViewById(R.id.textDestName);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }


//    private void setCollapseListener() {
//        onOffsetChangedListener = new AppBarLayout.OnOffsetChangedListener() {
//            @Override
//            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
//                if (collapsing_toolbar.getHeight() + verticalOffset < 2 * ViewCompat.getMinimumHeight(collapsing_toolbar)) {
//                    collapsing_toolbar.setTitle("");
//                } else {
//                    collapsing_toolbar.setTitle(getResources().getString(R.string.app_name));
//                }
//            }
//        };
//
//        appBar.addOnOffsetChangedListener(onOffsetChangedListener);
//    }

    private void getStationNameSource(String stationCode, TextView textViewName) {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(TrainUtils.STATION_CODE_TO_NAME)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        APIService service = retrofit.create(APIService.class);

        Call<AllStationModel> call = service.getTrainStationInfo(stationCode);

        call.enqueue(new Callback<AllStationModel>() {
            @Override
            public void onResponse(Call<AllStationModel> call, Response<AllStationModel> response) {
                if (response.body().isStatus()) {
                    textViewName.setText(response.body().getStationData().get(0).getName_en());
                    latitude1 = Double.parseDouble(response.body().getStationData().get(0).getLatitude());
                    longitude1 = Double.parseDouble(response.body().getStationData().get(0).getLongitude());

                    String time = getIntent().getStringExtra(SearchTrainsAdapter.TIME_DURATION_TIME);
                    SimpleDateFormat sdf = new SimpleDateFormat("hh:mm:ss");
                    try {
                        Date dt = sdf.parse(time);
                        SimpleDateFormat sdfs = new SimpleDateFormat("hh:mm a");
                        formatedTimeSource = sdfs.format(dt);
                        textSourceTime.setText(formatedTimeSource);
                        Log.e("TAG", "onResponse: " + "Distance : " + calculateDistanceInKM(latitude1, longitude1, latitude2, longitude2) + "\n" + formatedTimeSource);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }

                } else {
                    Toast.makeText(TrainsByNoActivity.this, response.message(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<AllStationModel> call, Throwable t) {
                Log.e("TAG", "onFailureStation : " + t.getMessage());
            }
        });

    }

    private void getStationNameDestination(String stationCode, TextView textViewName) {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(TrainUtils.STATION_CODE_TO_NAME)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        APIService service = retrofit.create(APIService.class);

        Call<AllStationModel> call = service.getTrainStationInfo(stationCode);

        call.enqueue(new Callback<AllStationModel>() {
            @Override
            public void onResponse(Call<AllStationModel> call, Response<AllStationModel> response) {
                if (response.body().isStatus()) {
                    textViewName.setText(response.body().getStationData().get(0).getName_en());
                    latitude2 = Double.parseDouble(response.body().getStationData().get(0).getLatitude());
                    longitude2 = Double.parseDouble(response.body().getStationData().get(0).getLongitude());

                    distance.setText(calculateDistanceInKM(latitude1, longitude1, latitude2, longitude2) + " km");
                    String time = getIntent().getStringExtra(SearchTrainsAdapter.TRAIN_DESTINATION_TIME);
                    SimpleDateFormat sdf = new SimpleDateFormat("hh:mm:ss");
                    try {
                        Date dt = sdf.parse(time);
                        SimpleDateFormat sdfs = new SimpleDateFormat("hh:mm a");
                        formatedTimeDest = sdfs.format(dt);
                        textDestTime.setText(formatedTimeDest);
                        duration.setText(getDuration(textSourceTime.getText().toString(), textDestTime.getText().toString()));
                        Log.e("TAG", "onResponse: " + "Distance : " + calculateDistanceInKM(latitude1, longitude1, latitude2, longitude2) + "\n" + formatedTimeDest);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }


                } else {
                    Toast.makeText(TrainsByNoActivity.this, response.message(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<AllStationModel> call, Throwable t) {
                Log.e("TAG", "onFailureStation : " + t.getMessage());
            }
        });

    }

    private String calculateDistanceInKM(double lat1, double long1, double lat2, double long2) {
        return new DecimalFormat("#.##").format(CalculationByDistance(new LatLng(lat1, long1)
                , new LatLng(lat2, long2)));
    }

    private String getDuration(String sourceTime, String destTime) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("hh:mm a");

        try {
            Date date1 = simpleDateFormat.parse(sourceTime);
            Date date2 = simpleDateFormat.parse(destTime);

            long difference = date2.getTime() - date1.getTime();
            int days = (int) (difference / (1000 * 60 * 60 * 24));
            int hours = (int) ((difference - (1000 * 60 * 60 * 24 * days)) / (1000 * 60 * 60));
            int min = (int) (difference - (1000 * 60 * 60 * 24 * days) - (1000 * 60 * 60 * hours)) / (1000 * 60);
            hours = (hours < 0 ? -hours : hours);
            min = (min < 0 ? -min : min);
            Log.e("======= Hours", " :: " + hours + min);
            timeDuration = hours + " h " + min + " min";
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return timeDuration;
    }

    double CalculationByDistance(LatLng StartP, LatLng EndP) {
        double lat1 = StartP.latitude;
        double lat2 = EndP.latitude;
        double lon1 = StartP.longitude;
        double lon2 = EndP.longitude;
        double dLat = Math.toRadians(lat2 - lat1);
        double dLon = Math.toRadians(lon2 - lon1);
        double valueResult = ((double) 6371) * (2.0d * Math.asin(Math.sqrt((Math.sin(dLat / 2.0d) * Math.sin(dLat / 2.0d)) + (((Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2))) * Math.sin(dLon / 2.0d)) * Math.sin(dLon / 2.0d)))));
        double meter = valueResult % 1000.0d;
        return meter;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.running_card:
                break;
            case R.id.route_card:
                break;

            case R.id.seat_card:
                break;

            case R.id.coach_card:
                break;
        }
    }
}