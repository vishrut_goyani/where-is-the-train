package com.demo.whereismytrain.activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.viewpager.widget.ViewPager;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.CheckBox;
import android.widget.RelativeLayout;

import com.demo.whereismytrain.R;
import com.demo.whereismytrain.TrainApplication;
import com.demo.whereismytrain.adapter.HomeModulesAdapter;
import com.demo.whereismytrain.adapter.ViewPagerHomeAdapter;
import com.demo.whereismytrain.interfaces.APIService;
import com.demo.whereismytrain.model.HomeScreenModules;
import com.demo.whereismytrain.model.OfficeModel;
import com.demo.whereismytrain.model.ViewPagerModel;
import com.demo.whereismytrain.utils.TrainUtils;
import com.demo.whereismytrain.widgets.BaseRecyclerView;
import com.demo.whereismytrain.widgets.CustomLinearLayoutManager;
import com.google.android.material.bottomappbar.BottomAppBar;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.navigation.NavigationView;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.File;
import java.util.ArrayList;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.Retrofit.Builder;
import retrofit2.converter.gson.GsonConverterFactory;

public class HomeMainActivity extends AppCompatActivity {

    BaseRecyclerView rv_home_modules;
    HomeModulesAdapter homeModulesAdapter;
    ArrayList<HomeScreenModules> arrayList;
    ArrayList<ViewPagerModel> pagerModelArrayList;
    ViewPager viewPagerHome;
    ViewPagerHomeAdapter viewPagerHomeAdapter;

    SharedPreferences preferences;
    SharedPreferences.Editor editor;
    public static final String nightModeEnabled = "isNightModeEnabled";
    public static final int STORAGE_CODE = 1001;

    int[] images = new int[]{
            R.drawable.ic_publish_white,
            R.drawable.ic_publish_white,
            R.drawable.ic_publish_white,
            R.drawable.ic_publish_white,
            R.drawable.ic_publish_white,
            R.drawable.ic_publish_white,
            R.drawable.ic_publish_white,
            R.drawable.ic_publish_white,
            R.drawable.ic_publish_white
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        init();
        new LoadModules().execute("");

    }

    private void init() {
        arrayList = new ArrayList<>();
        pagerModelArrayList = new ArrayList<>();
        rv_home_modules = findViewById(R.id.rv_home_modules);
        viewPagerHome = findViewById(R.id.viewPagerHome);
        RelativeLayout liveStatus = findViewById(R.id.liveStatus);
        RelativeLayout pnrStatus = findViewById(R.id.pnrStatus);
        NavigationView materialNavigationView = findViewById(R.id.materialNavigationView);
        DrawerLayout drawerLayout = findViewById(R.id.drawerLayout);
        FloatingActionButton fabBookTicket = findViewById(R.id.fabBookTicket);
        BottomAppBar bottom_app_bar = findViewById(R.id.bottom_app_bar);
        CheckBox chk_night_mode = findViewById(R.id.chk_night_mode);
        Toolbar toolbar = findViewById(R.id.toolbar);

        preferences = getSharedPreferences(TrainApplication.APP_PREFS, MODE_PRIVATE);
        editor = preferences.edit();

        ActionBarDrawerToggle actionBarDrawerToggle = new ActionBarDrawerToggle(
                this, drawerLayout, toolbar, R.string.drawer_open, R.string.drawer_close);

        drawerLayout.addDrawerListener(actionBarDrawerToggle);
        actionBarDrawerToggle.syncState();


        liveStatus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });

        pnrStatus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });

        fabBookTicket.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!checkStoragePermission()) {
                    requestStoragePermission();
                } else {
                    File file = new File(Environment.getExternalStorageDirectory().toString() + File.separator + "Students Saturday Schedule.xlsx");
                    Uri uri = FileProvider.getUriForFile(HomeMainActivity.this, getPackageName() + ".myprovider", file);
                    grantUriPermission(getPackageName(), uri, Intent.FLAG_GRANT_READ_URI_PERMISSION);
                    RequestBody body = RequestBody.create(MediaType.parse("multipart/form-data"), file);

                    getConvertedFile(body);
                }
            }
        });

        if (!preferences.getBoolean(nightModeEnabled, false)) {
            chk_night_mode.setChecked(false);
        } else {
            chk_night_mode.setChecked(true);
        }

        chk_night_mode.setOnCheckedChangeListener(null);
        chk_night_mode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (preferences.getBoolean(nightModeEnabled, false)) {
                    AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
                    editor.putBoolean(nightModeEnabled, false);
                    editor.apply();
                } else {
                    AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
                    editor.putBoolean(nightModeEnabled, true);
                    editor.apply();
                }
            }
        });

        materialNavigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                switch (menuItem.getItemId()) {
                    case R.id.nav_language:
                        Log.e("TAG", "onNavigationItemSelected: ");
                        return true;
                    case R.id.nav_settings:
                        Log.e("TAG", "onNavigationItemSelected: ");
                        return true;
                    case R.id.nav_share:
                        Log.e("TAG", "onNavigationItemSelected: ");
                        return true;
                    case R.id.nav_suggest_feature:
                        Log.e("TAG", "onNavigationItemSelected: ");
                        return true;
                    case R.id.nav_about:
                        Log.e("TAG", "onNavigationItemSelected: ");
                        return true;
                    default:
                        return true;
                }
            }
        });
        CustomLinearLayoutManager manager = new CustomLinearLayoutManager(this, 3);
        manager.setScrollEnabled(false);
        rv_home_modules.setLayoutManager(manager);
        rv_home_modules.setHasFixedSize(true);

    }

    private boolean checkStoragePermission() {
        if (ActivityCompat.checkSelfPermission(HomeMainActivity.this,
                Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(HomeMainActivity.this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            return false;
        } else {
            return true;
        }
    }

    private void requestStoragePermission() {
        ActivityCompat.requestPermissions(HomeMainActivity.this,
                new String[]{
                        Manifest.permission.READ_EXTERNAL_STORAGE,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE
                }, STORAGE_CODE);
    }

    private void getConvertedFile(RequestBody body) {
        ProgressDialog pd = new ProgressDialog(this);
        pd.setTitle("Converting...");
        pd.setCancelable(false);
        pd.show();

        Gson gson = new GsonBuilder().setLenient().create();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(TrainUtils.PDF_BASE_URL)
                .addConverterFactory(GsonConverterFactory.create()).build();

        APIService apiService = retrofit.create(APIService.class);
        Call<OfficeModel> call = apiService.getConvertedPdf(body);
        call.enqueue(new Callback<OfficeModel>() {
            @Override
            public void onResponse(Call<OfficeModel> call, Response<OfficeModel> response) {
                pd.dismiss();
                if (response.body().getState().equals("SUCCESS"))
                    Log.e("DONETAG", "onResponse: " + response.body().getFilename());
                else
                    Log.e("FAILTAG", "onResponse: " + response.toString());
            }

            @Override
            public void onFailure(Call<OfficeModel> call, Throwable t) {
                pd.dismiss();
                Log.e("FAILTAG", "onFailure: " + t.getMessage());
            }
        });
    }

    private class LoadModules extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... params) {
            for (int i = 0; i < 9; i++) {
                HomeScreenModules homeScreenModules = new HomeScreenModules();
                homeScreenModules.setImage(images[i]);
                homeScreenModules.setNameText(getResources().getStringArray(R.array.home_modules)[i]);
                arrayList.add(homeScreenModules);
            }

            for (int i = 0; i < 4; i++) {
                pagerModelArrayList.add(new ViewPagerModel(R.drawable.ic_publish_white));
            }
            homeModulesAdapter = new HomeModulesAdapter(HomeMainActivity.this, arrayList);
            viewPagerHomeAdapter = new ViewPagerHomeAdapter(HomeMainActivity.this, pagerModelArrayList);
            return "Executed";
        }

        @Override
        protected void onPostExecute(String result) {
            rv_home_modules.setAdapter(homeModulesAdapter);
            viewPagerHome.setAdapter(viewPagerHomeAdapter);
        }

        @Override
        protected void onPreExecute() {
        }
    }

}