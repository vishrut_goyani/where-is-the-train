package com.demo.whereismytrain.activity;

import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.demo.whereismytrain.R;
import com.demo.whereismytrain.TrainApplication;
import com.demo.whereismytrain.adapter.SearchStationAdapter;
import com.demo.whereismytrain.adapter.SearchTrainsAdapter;
import com.demo.whereismytrain.database.DatabaseHelper;
import com.demo.whereismytrain.interfaces.APIService;
import com.demo.whereismytrain.model.traindao.AllStationModel;
import com.demo.whereismytrain.model.traindao.AllTrainModel;
import com.demo.whereismytrain.model.traindao.SearchStationModel;
import com.demo.whereismytrain.model.traindao.SearchTrainModel;
import com.demo.whereismytrain.widgets.HidingScrollListener;
import com.demo.whereismytrain.utils.TrainUtils;
import com.demo.whereismytrain.widgets.BaseRecyclerView;
import com.paulrybitskyi.persistentsearchview.PersistentSearchView;
import com.paulrybitskyi.persistentsearchview.listeners.OnSearchConfirmedListener;
import com.paulrybitskyi.persistentsearchview.listeners.OnSearchQueryChangeListener;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.demo.whereismytrain.database.DatabaseHelper.COL_STATION_CODE;
import static com.demo.whereismytrain.database.DatabaseHelper.COL_STATION_LAT;
import static com.demo.whereismytrain.database.DatabaseHelper.COL_STATION_LONG;
import static com.demo.whereismytrain.database.DatabaseHelper.COL_STATION_NAME;
import static com.demo.whereismytrain.database.DatabaseHelper.STATION_LIST_TABLE;

public class SearchStationsActivity extends AppCompatActivity {

    SearchStationAdapter searchStationAdapter;
    private InputMethodManager mImm;
    private BaseRecyclerView recyclerView;
    private ArrayList<AllStationModel.Station_Details> all_stations_list;
    private ArrayList<SearchStationModel> searchList;
    PersistentSearchView persistentSearchView;
    DatabaseHelper helper;
    SQLiteDatabase database;

    ProgressDialog progressDialog;
    SharedPreferences preferences;
    SharedPreferences.Editor editor;
    public static final String downloadedAllStations = "downloadedStations";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        progressDialog = new ProgressDialog(this);
        helper = new DatabaseHelper(this);
        database = helper.getWritableDatabase();
        preferences = getSharedPreferences(TrainApplication.APP_PREFS, MODE_PRIVATE);
        editor = preferences.edit();
        init();

    }

    private void init() {
        searchList = new ArrayList<>();
        all_stations_list = new ArrayList<>();
        mImm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);

        persistentSearchView = findViewById(R.id.persistentSearchView);
        recyclerView = findViewById(R.id.recyclerview);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.addOnScrollListener(new HidingScrollListener() {
            @Override
            public void onHide() {
                hideViews();
            }

            @Override
            public void onShow() {
                showViews();
            }
        });

        if (!preferences.getBoolean(downloadedAllStations, false))
            getAllStations();

        initSearch();

    }

    private void getAllStations() {
        progressDialog.setCancelable(false);
        progressDialog.setTitle("Loading...");
        progressDialog.setMessage("Fetching data from server...");
        progressDialog.show();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(TrainUtils.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        APIService service = retrofit.create(APIService.class);
        Call<AllStationModel> call = service.getAllStations();

        call.enqueue(new Callback<AllStationModel>() {
            @Override
            public void onResponse(Call<AllStationModel> call, Response<AllStationModel> response) {
                progressDialog.dismiss();
                if (response.body().isStatus()) {
                    all_stations_list = response.body().getStationData();
                    new SaveOfflineTask().execute(all_stations_list);
                } else {
                    Log.e("TAG", "onResponse: " + response.message());
                }
            }

            @Override
            public void onFailure(Call<AllStationModel> call, Throwable t) {
                progressDialog.dismiss();
                Log.e("TAG", "onFailure: " + t.getMessage());
            }
        });
    }

    private class SaveOfflineTask extends AsyncTask<ArrayList<AllStationModel.Station_Details>, Void, Void> {

        @Override
        protected Void doInBackground(ArrayList<AllStationModel.Station_Details>... arrayLists) {
            SQLiteDatabase db = helper.getWritableDatabase();
            db.beginTransaction();
            for (int i = 0; i < arrayLists[0].size(); i++) {
                ContentValues contentValues = new ContentValues();
                contentValues.put(COL_STATION_CODE, arrayLists[0].get(i).getStation_code());
                contentValues.put(COL_STATION_NAME, arrayLists[0].get(i).getName_en());
                contentValues.put(COL_STATION_LAT, arrayLists[0].get(i).getLatitude());
                contentValues.put(COL_STATION_LONG, arrayLists[0].get(i).getLongitude());
                db.insert(STATION_LIST_TABLE, null, contentValues);

            }
            db.setTransactionSuccessful();
            db.endTransaction();
            return null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog.setCancelable(false);
            progressDialog.setTitle("Loading...");
            progressDialog.setMessage("Saving data...");
            progressDialog.show();
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            editor.putBoolean(downloadedAllStations, true);
            editor.apply();
            progressDialog.dismiss();
            persistentSearchView.requestFocus();
        }
    }

    private void initSearch() {
        persistentSearchView.requestFocus();
        persistentSearchView.setOnLeftBtnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        persistentSearchView.setOnSearchConfirmedListener(new OnSearchConfirmedListener() {
            @Override
            public void onSearchConfirmed(PersistentSearchView searchView, String query) {
                hideInputManager();
            }
        });
        persistentSearchView.setOnSearchQueryChangeListener(new OnSearchQueryChangeListener() {
            @Override
            public void onSearchQueryChanged(PersistentSearchView searchView, String oldQuery, String newQuery) {
                if (!TextUtils.isEmpty(newQuery)) {
                    getStationsOffline(newQuery);
                }
            }
        });
    }

    private void getStationsOffline(String newQuery) {
        searchList.clear();
        searchList = new ArrayList<>();

        Cursor cursor = database.rawQuery(" SELECT * FROM " + STATION_LIST_TABLE +
                " WHERE " + COL_STATION_CODE + " LIKE '" + newQuery + "%' OR "
                + COL_STATION_NAME + " LIKE '" + newQuery + "%'", null);

        if (cursor != null) {
            if (cursor.moveToFirst()) {
                do {
                    String station_no = cursor.getString(cursor.getColumnIndex(COL_STATION_CODE));
                    String station_name = cursor.getString(cursor.getColumnIndex(COL_STATION_NAME));
                    String station_lat = cursor.getString(cursor.getColumnIndex(COL_STATION_LAT));
                    String station_long = cursor.getString(cursor.getColumnIndex(COL_STATION_LONG));

                    SearchStationModel searchTrainModel = new SearchStationModel(
                            station_name,
                            station_no,
                            station_lat,
                            station_long
                    );
                    searchList.add(searchTrainModel);
                } while (cursor.moveToNext());
            }
            searchStationAdapter = new SearchStationAdapter(this, searchList);
            recyclerView.setAdapter(searchStationAdapter);
            cursor.close();
        }
    }

    private void hideViews() {
        persistentSearchView.animate().translationY(-persistentSearchView.getHeight()).setInterpolator(new AccelerateInterpolator(2));
    }

    private void showViews() {
        persistentSearchView.animate().translationY(0).setInterpolator(new DecelerateInterpolator(2));
    }

    public void hideInputManager() {
        if (persistentSearchView != null) {
            if (mImm != null) {
                mImm.hideSoftInputFromWindow(persistentSearchView.getWindowToken(), 0);
            }
            persistentSearchView.clearFocus();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}