package com.demo.whereismytrain.utils;

import android.app.Activity;
import android.content.Intent;

import com.demo.whereismytrain.activity.SearchTrainsActivity;

public class NavigationUtils {

    public static void navigateToSearch(Activity context) {
        final Intent intent = new Intent(context, SearchTrainsActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        intent.setAction(TrainUtils.NAVIGATE_SEARCH);
        context.startActivity(intent);
    }

}
