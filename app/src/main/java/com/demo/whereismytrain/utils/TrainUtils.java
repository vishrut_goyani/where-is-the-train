package com.demo.whereismytrain.utils;

public class TrainUtils {
    public static final String BASE_URL = "http://punchapp.in/api/";
    public static final String API_KEY = "8c89620f137c5f8195eef18a85faea4e";
    public static final String ALL_TRAIN = "all-train-list";
    public static final String ALL_STATION = "all-station-list";
    public static final String TRAIN_NO_ROUTE = "https://premlyvideostatus.com/api/";
    public static final String TRAIN_NO_AUTOCOMPLETE = "https://premlyvideostatus.com/api/";
    public static final String TRAIN_STATION_AUTOCOMPLETE = "https://premlyvideostatus.com/api/";
    public static final String STATION_CODE_TO_NAME = "https://premlyvideostatus.com/api/";
    public static final String STATION_TRAINS_LIST = "http://indianrailapi.com/api/v2/AllTrainOnStation/apikey/8c89620f137c5f8195eef18a85faea4e/";
    public static final String TRAIN_COACH = "http://indianrailapi.com/api/v2/CoachLayout/apikey/8c89620f137c5f8195eef18a85faea4e/";
    public static final String STATION_LOCATION = "http://indianrailapi.com/api/v2/StationLocationOnMap/apikey/8c89620f137c5f8195eef18a85faea4e/";

    public static final String PARTIALLY_CANCELLED_TRAINS = "https://indianrailapi.com/api/v2/PartiallyCancelledTrains/apikey/8c89620f137c5f8195eef18a85faea4e/Date/";
    public static final String CANCELLED_TRAINS = "https://indianrailapi.com/api/v2/CancelledTrains/apikey/8c89620f137c5f8195eef18a85faea4e/Date/";
    public static final String DIVERTED_TRAINS = "https://indianrailapi.com/api/v2/DivertedTrains/apikey/8c89620f137c5f8195eef18a85faea4e/Date/";
    public static final String RESCHEDULED_TRAINS = "https://indianrailapi.com/api/v2/RescheduledTrains/apikey/8c89620f137c5f8195eef18a85faea4e/Date/";

    public static final String NAVIGATE_SEARCH = "navigate_search";

    public static final String PDF_BASE_URL = "https://s8.aconvert.com/convert/";
}





























