package com.demo.whereismytrain.database;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.Nullable;

public class DatabaseHelper extends SQLiteOpenHelper {

    public static final String DATABASE_NAME = "WhereIsMyTrain";
    public static final String TRAIN_LIST_TABLE = "train_list_table";
    public static final String STATION_LIST_TABLE = "station_list_table";

    public static final String COL_ID_TRAIN = "TRAIN_ID";
    public static final String COL_TRAIN_NO = "TRAIN_NO";
    public static final String COL_TRAIN_NAME = "TRAIN_NAME";
    public static final String COL_SOURCE_CODE = "SOURCE_CODE";
    public static final String COL_DESTINATION_CODE = "DESTINATION_CODE";
    public static final String COL_SOURCE_NAME = "SOURCE_NAME";
    public static final String COL_DESTINATION_NAME = "DESTINATION_NAME";

    public static final String COL_ID_STATION = "STATION_ID";
    public static final String COL_STATION_CODE = "STATION_CODE";
    public static final String COL_STATION_NAME = "STATION_NAME";
    public static final String COL_STATION_LAT = "STATION_LATITUDE";
    public static final String COL_STATION_LONG = "STATION_LONGITUDE";

    private static final String CREATE_TABLE_TRAIN =
            "CREATE TABLE IF NOT EXISTS " + TRAIN_LIST_TABLE
                    + "(" + COL_ID_TRAIN + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                    + COL_TRAIN_NO + " TEXT,"
                    + COL_TRAIN_NAME + " TEXT,"
                    + COL_SOURCE_CODE + " TEXT,"
                    + COL_DESTINATION_CODE + " TEXT,"
                    + COL_SOURCE_NAME + " TEXT,"
                    + COL_DESTINATION_NAME + " TEXT" + ")";

    private static final String CREATE_TABLE_STATION =
            "CREATE TABLE IF NOT EXISTS " + STATION_LIST_TABLE
                    + "(" + COL_ID_STATION + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                    + COL_STATION_CODE + " TEXT,"
                    + COL_STATION_NAME + " TEXT,"
                    + COL_STATION_LAT + " TEXT,"
                    + COL_STATION_LONG + " TEXT" + ")";

    public DatabaseHelper(@Nullable Context context) {
        super(context, DATABASE_NAME, null, 3);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(CREATE_TABLE_TRAIN);
        sqLiteDatabase.execSQL(CREATE_TABLE_STATION);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + TRAIN_LIST_TABLE);
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + STATION_LIST_TABLE);
        onCreate(sqLiteDatabase);
    }

    /*public void insertAllTrains(String train_no, String train_name, String train_source, String train_destination) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(COL_TRAIN_NO, train_no);
        contentValues.put(COL_TRAIN_NAME, train_name);
        contentValues.put(COL_TRAIN_SOURCE, train_source);
        contentValues.put(COL_TRAIN_DESTINATION, train_destination);
        long result = db.insert(TRAIN_LIST_TABLE, null, contentValues);
    }*/

    public Cursor getAllTrains() {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor res = db.rawQuery("SELECT * FROM " + TRAIN_LIST_TABLE, null);
        return res;
    }

    public Cursor getAllStations() {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor res = db.rawQuery("SELECT * FROM " + STATION_LIST_TABLE, null);
        return res;
    }

}
