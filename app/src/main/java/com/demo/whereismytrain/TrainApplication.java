package com.demo.whereismytrain;

import android.app.Application;
import android.content.SharedPreferences;

import androidx.appcompat.app.AppCompatDelegate;

import com.demo.whereismytrain.activity.HomeMainActivity;

public class TrainApplication extends Application {

    SharedPreferences preferences;
    SharedPreferences.Editor editor;
    public static final String APP_PREFS = "WhereIsMyTrain";
    static TrainApplication instance = new TrainApplication();

    public static TrainApplication getInstance() {
        return instance;
    }


    @Override
    public void onCreate() {
        super.onCreate();

        preferences = getSharedPreferences(APP_PREFS, MODE_PRIVATE);
        editor = preferences.edit();

        if (!preferences.getBoolean(HomeMainActivity.nightModeEnabled, false)) {
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
            editor.putBoolean(HomeMainActivity.nightModeEnabled, false);
            editor.apply();
        } else {
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
            editor.putBoolean(HomeMainActivity.nightModeEnabled, true);
            editor.apply();
        }

    }
}
