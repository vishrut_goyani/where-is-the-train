package com.demo.whereismytrain.model.traindao;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class AllStationModel {

    @SerializedName("status")
    boolean status;

    @SerializedName("message")
    String message;

    @SerializedName("data")
    ArrayList<Station_Details> data;

    public static class Station_Details {
        @SerializedName("id")
        int id;

        @SerializedName("name_en")
        String name_en;

        @SerializedName("name_hn")
        String name_hn;

        @SerializedName("station_code")
        String station_code;

        @SerializedName("longitude")
        String longitude;

        @SerializedName("latitude")
        String latitude;

        public int getId() {
            return id;
        }

        public String getName_en() {
            return name_en;
        }

        public String getName_hn() {
            return name_hn;
        }

        public String getStation_code() {
            return station_code;
        }

        public String getLongitude() {
            return longitude;
        }

        public String getLatitude() {
            return latitude;
        }
    }

    public ArrayList<Station_Details> getStationData() {
        return data;
    }

    public boolean isStatus() {
        return status;
    }

    public String getMessage() {
        return message;
    }

}
