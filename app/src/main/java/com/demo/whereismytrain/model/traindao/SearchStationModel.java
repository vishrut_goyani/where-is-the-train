package com.demo.whereismytrain.model.traindao;

public class SearchStationModel {

    String name_en, station_code, longitude, latitude;

    public SearchStationModel() {
    }

    public SearchStationModel(String name_en, String station_code, String longitude, String latitude) {
        this.name_en = name_en;
        this.station_code = station_code;
        this.longitude = longitude;
        this.latitude = latitude;
    }

    public String getName_en() {
        return name_en;
    }

    public void setName_en(String name_en) {
        this.name_en = name_en;
    }

    public String getStation_code() {
        return station_code;
    }

    public void setStation_code(String station_code) {
        this.station_code = station_code;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }
}
