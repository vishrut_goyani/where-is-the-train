package com.demo.whereismytrain.model.traindao;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class StationTrainsModel {

    @SerializedName("ResponseCode")
    String ResponseCode;

    @SerializedName("Status")
    String Status;

    @SerializedName("Trains")
    ArrayList<Trains_Details> Trains;

    public static class Trains_Details{
        @SerializedName("TrainNo")
        String TrainNo;

        @SerializedName("TrainName")
        String TrainName;

        @SerializedName("Source")
        String Source;

        @SerializedName("ArrivalTime")
        String ArrivalTime;

        @SerializedName("Destination")
        String Destination;

        @SerializedName("DepartureTime")
        String DepartureTime;

        public String getTrainNo() {
            return TrainNo;
        }

        public String getTrainName() {
            return TrainName;
        }

        public String getSource() {
            return Source;
        }

        public String getArrivalTime() {
            return ArrivalTime;
        }

        public String getDestination() {
            return Destination;
        }

        public String getDepartureTime() {
            return DepartureTime;
        }
    }

    public String getResponseCode() {
        return ResponseCode;
    }

    public String getStatus() {
        return Status;
    }

    public ArrayList<Trains_Details> getTrains() {
        return Trains;
    }
}
