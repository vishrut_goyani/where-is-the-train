package com.demo.whereismytrain.model.traindao;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class AllTrainModel {

    @SerializedName("status")
    boolean status;

    @SerializedName("message")
    String message;

    @SerializedName("data")
    @Expose
    ArrayList<Trains_Data> data;

    public static class Trains_Data {
        @SerializedName("train_no")
        @Expose
        String train_no;

        @SerializedName("train_name")
        @Expose
        String train_name;

        @SerializedName("train_source")
        @Expose
        String source;

        @SerializedName("destination_source")
        @Expose
        String destination;

        @SerializedName("source_station_name")
        @Expose
        String source_station_name;

        @SerializedName("destination_train_station")
        @Expose
        String destination_train_station;

        public String getSource() {
            return source;
        }

        public String getDestination() {
            return destination;
        }

        public String getTrain_no() {
            return train_no;
        }

        public String getTrain_name() {
            return train_name;
        }

        public String getSource_station_name() {
            return source_station_name;
        }

        public String getDestination_train_station() {
            return destination_train_station;
        }
    }

    public boolean getStatus() {
        return status;
    }

    public String getMessage() {
        return message;
    }

    public ArrayList<Trains_Data> getData() {
        return data;
    }
}
