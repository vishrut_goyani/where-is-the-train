package com.demo.whereismytrain.model;

import com.google.gson.annotations.SerializedName;

public class OfficeModel {

    @SerializedName("filename")
    String filename;

    @SerializedName("state")
    String state;

    public String getFilename() {
        return filename;
    }

    public String getState() {
        return state;
    }
}
