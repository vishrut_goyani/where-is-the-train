package com.demo.whereismytrain.model.traindao;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class CoachModel {

    @SerializedName("ResponseCode")
    String ResponseCode;

    @SerializedName("TrainNumber")
    String TrainNumber;

    @SerializedName("Coaches")
    ArrayList<Coach_Details> Coaches;

    public static class Coach_Details {
        @SerializedName("SerialNo")
        String SerialNo;

        @SerializedName("Code")
        String Code;

        @SerializedName("Name")
        String Name;

        @SerializedName("Number")
        String Number;

        int type;

        public String getSerialNo() {
            return SerialNo;
        }

        public String getCode() {
            return Code;
        }

        public String getName() {
            return Name;
        }

        public String getNumber() {
            return Number;
        }

        public void setType(int type) {
            this.type = type;
        }

        public int getType() {
            return type;
        }
    }

    public String getResponseCode() {
        return ResponseCode;
    }

    public String getTrainNumber() {
        return TrainNumber;
    }

    public ArrayList<Coach_Details> getCoaches() {
        return Coaches;
    }
}
