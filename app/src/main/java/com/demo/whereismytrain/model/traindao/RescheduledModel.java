package com.demo.whereismytrain.model.traindao;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class RescheduledModel {

    @SerializedName("ResponseCode")
    String ResponseCode;

    @SerializedName("TotalTrain")
    String TotalTrain;

    @SerializedName("Trains")
    ArrayList<Trains_Details> Trains;

    public static class Trains_Details {
        @SerializedName("TrainNumber")
        String TrainNumber;

        @SerializedName("TrainName")
        String TrainName;

        @SerializedName("StartDate")
        String StartDate;

        @SerializedName("TrainType")
        String TrainType;

        @SerializedName("Source")
        String Source;

        @SerializedName("Destination")
        String Destination;

        @SerializedName("RescheduledBy")
        String RescheduledBy;

        @SerializedName("RescheduledDate")
        String RescheduledDate;

        @SerializedName("RescheduledTime")
        String RescheduledTime;

        public String getTrainNumber() {
            return TrainNumber;
        }

        public String getTrainName() {
            return TrainName;
        }

        public String getStartDate() {
            return StartDate;
        }

        public String getTrainType() {
            return TrainType;
        }

        public String getSource() {
            return Source;
        }

        public String getDestination() {
            return Destination;
        }

        public String getRescheduledBy() {
            return RescheduledBy;
        }

        public String getRescheduledDate() {
            return RescheduledDate;
        }

        public String getRescheduledTime() {
            return RescheduledTime;
        }
    }

    public String getResponseCode() {
        return ResponseCode;
    }

    public String getTotalTrain() {
        return TotalTrain;
    }

    public ArrayList<Trains_Details> getTrains() {
        return Trains;
    }

}
