package com.demo.whereismytrain.model.traindao;

import com.google.gson.annotations.SerializedName;

public class TrainNoModel {

    @SerializedName("ResponseCode")
    String responseCode;

    @SerializedName("Status")
    String Status;

    @SerializedName("TrainNo")
    String TrainNo;

    @SerializedName("TrainName")
    String TrainName;

    @SerializedName("Source")
    Source Source;

    @SerializedName("Destination")
    Destination Destination;

    public static class Source{
        @SerializedName("Code")
        String Code;

        @SerializedName("Arrival")
        String Arrival;

        public String getCode() {
            return Code;
        }

        public String getArrival() {
            return Arrival;
        }
    }

    public static class Destination{
        @SerializedName("Code")
        String Code;

        @SerializedName("Arrival")
        String Arrival;

        public String getCode() {
            return Code;
        }

        public String getArrival() {
            return Arrival;
        }
    }

    @SerializedName("Message")
    String Message;

    public String getResponseCode() {
        return responseCode;
    }

    public String getStatus() {
        return Status;
    }

    public String getTrainNo() {
        return TrainNo;
    }

    public String getTrainName() {
        return TrainName;
    }

    public Source getSource() {
        return Source;
    }

    public Destination getDestination() {
        return Destination;
    }

    public String getMessage() {
        return Message;
    }
}
