package com.demo.whereismytrain.model.traindao;

public class SearchTrainModel {

    String train_no, train_name, source_code, destintion_code, source_name, destination_name;

    public SearchTrainModel() {
    }

    public SearchTrainModel(String train_no, String train_name, String source_code, String destintion_code, String source_name, String destination_name) {
        this.train_no = train_no;
        this.train_name = train_name;
        this.source_code = source_code;
        this.destintion_code = destintion_code;
        this.source_name = source_name;
        this.destination_name = destination_name;
    }

    public String getTrain_no() {
        return train_no;
    }

    public void setTrain_no(String train_no) {
        this.train_no = train_no;
    }

    public String getTrain_name() {
        return train_name;
    }

    public void setTrain_name(String train_name) {
        this.train_name = train_name;
    }

    public String getSource_code() {
        return source_code;
    }

    public void setSource_code(String source_code) {
        this.source_code = source_code;
    }

    public String getDestintion_code() {
        return destintion_code;
    }

    public void setDestintion_code(String destintion_code) {
        this.destintion_code = destintion_code;
    }

    public String getSource_name() {
        return source_name;
    }

    public void setSource_name(String source_name) {
        this.source_name = source_name;
    }

    public String getDestination_name() {
        return destination_name;
    }

    public void setDestination_name(String destination_name) {
        this.destination_name = destination_name;
    }
}
