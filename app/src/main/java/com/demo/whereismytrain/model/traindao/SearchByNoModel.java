package com.demo.whereismytrain.model.traindao;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class SearchByNoModel {

    @SerializedName("status")
    boolean status;

    @SerializedName("message")
    String message;

    @SerializedName("data")
    Trains_Data data;

    public static class Trains_Data {

        @SerializedName("current_page")
        int current_page;

        @SerializedName("data")
        ArrayList<Trains_Details> data;

        public static class Trains_Details {
            @SerializedName("id")
            int id;

            @SerializedName("train_no")
            String train_no;

            @SerializedName("train_name")
            String train_name;

            @SerializedName("train_source")
            String train_source;

            @SerializedName("destination_source")
            String destination_source;

            @SerializedName("source_arrival")
            String source_arrival;

            @SerializedName("destination_arrival")
            String destination_arrival;

            @SerializedName("source")
            Source source;

            @SerializedName("destination")
            Destination destination;

            public static class Source {
                @SerializedName("name_en")
                String name_en;

                public String getName_en() {
                    return name_en;
                }
            }

            public static class Destination {
                @SerializedName("name_en")
                String name_en;

                public String getName_en() {
                    return name_en;
                }
            }

            public int getId() {
                return id;
            }

            public String getTrain_no() {
                return train_no;
            }

            public String getTrain_name() {
                return train_name;
            }

            public String getTrain_source() {
                return train_source;
            }

            public String getDestination_source() {
                return destination_source;
            }

            public String getSource_arrival() {
                return source_arrival;
            }

            public String getDestination_arrival() {
                return destination_arrival;
            }

            public Source getSource() {
                return source;
            }

            public Destination getDestination() {
                return destination;
            }
        }

        public int getCurrent_page() {
            return current_page;
        }

        public ArrayList<Trains_Details> getTrainData() {
            return data;
        }
    }

    public boolean isStatus() {
        return status;
    }

    public String getMessage() {
        return message;
    }

    public Trains_Data getData() {
        return data;
    }
}
